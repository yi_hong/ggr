%clear G;
%clear Q;

%rng(10);

%m = 10;
%N = 10;
%d = 3;
%G = grassmannfactory(N,d);

%Q = cell(m,1);
%for i=1:m
%    Q{i} = G.rand();
%end

%rs = randsample(1:N,2);

%p0 = Q{rs(1)};
%p1 = Q{rs(2)};
%v0 = G.log(p0,p1);

%t = linspace(0,1,m);
%[p,v] = gd(Q,p0,v0,t,1,50,0.5);

clear cc
load ./data/cc.mat

xAge = cc.ages;
time = (xAge-min(xAge))./(max(xAge(:) - min(xAge(:))));
m = length(time);
 
Q = cell(m,1);
sigma = zeros(size(cc.data{1},2), size(cc.data{1},2));
for i=1:m
    [U,sTmp,~] = svd(cc.data{i},0);
    Q{i} = U;
    sigma = sigma + sTmp;
end
sigma = sigma ./ m;

tic 

% pick initial geodesic
[p0,v0] = cdc_init(Q,time,50);

% run GD algorithm
[p_opt,v_opt] = cdc_gd(Q,p0,v0,time,1e-14,50,0.5);

toc

N = size(Q{1},1); % pts
d = size(Q{1},2); % dim
G = grassmannfactory(N,d);

figure;
plotPoints = 50;
color = jet(plotPoints);
spac = linspace(0,1,plotPoints);

% compare with GGR
addpath(genpath('../'));
tmp = load('/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714/corpus_callosum/corpus_callosum_Part1Of1.mat');
pointGGR = plot2DShapeOnGeodesic( tmp.initPnt, tmp.initVel, tmp.tPos, ...
    tmp.timeWarpParams, min(cc.ages), max(cc.ages), sigma, plotPoints, tmp.grParams, 1 );

% just for test to plot the distance between original data points
distMap = zeros(length(Q), length(Q));
for i = 1:length(Q)
    for j = 1:length(Q)
        distMap(i,j) = grarc(Q{i}, Q{j});
    end
    distMap(i,i) = NaN;
end
figure, imagesc(distMap);
colormap(cool);

subplot(1,2,1), hold on;
for i=1:plotPoints
    x = pointGGR{i}*sigma;
    x(end+1,:) = x(1,:);
    plot(x(:,1),x(:,2), ...
        'color',color(i,:), ...
        'linewidth',1.5);
end
caxis([min(cc.ages) max(cc.ages)]);
colorbar();
box off;
axis off;
title('GGR');
set(gca,'xdir','normal','ydir','reverse');
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 3])

pointCDC = cell(plotPoints, 1);
subplot(1, 2, 2), hold on;
for i=1:plotPoints
    x = G.exp(p_opt,spac(i)*v_opt);
    pointCDC{i} = x;
    x = x * sigma;
    x(end+1,:) = x(1,:);
    plot(x(:,1),x(:,2), ...
        'color',color(i,:), ...
        'linewidth',1.5);
end

%caxis([min(time) max(time)]); 
caxis([min(cc.ages) max(cc.ages)]);
colorbar();
box off;
axis off;
title('CDC');
% ECCV compatible view
set(gca,'xdir','normal','ydir','reverse');
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 3])
print -depsc '/tmp/output.eps'
%close;

assert(length(pointGGR) == length(pointCDC));
figure;
distErr = zeros(length(pointGGR), 1);
for i=1:length(pointGGR)
    %pointGGR{i}' * pointGGR{i}
    %pointCDC{i}' * pointCDC{i}
    distErr(i) = grarc(pointGGR{i}, pointCDC{i});
end
plot(spac, distErr);








