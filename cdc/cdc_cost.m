function f = cdc_cost(G,Q,p,v,t)
    m = length(Q);
    f = 0;
    for i=1:m
        p_i = G.exp(p,v,t(i));
        q_i = Q{i};
        f=f + cdc_grDistSq(q_i,p_i);
    end
end