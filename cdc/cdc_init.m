function [p0_opt,v0_opt] = gdInit(Q,t,n_iter)
    m = length(Q);
    assert(m>1);
    N = size(Q{1},1);
    d = size(Q{1},2);
    
    % helper tools
    G = grassmannfactory(N,d);
   
    p0_opt = Inf;
    v0_opt = Inf;
    
    E = Inf;
    for i=1:n_iter
        % pick two points
        sel = randsample(1:m,2);
        p0 = Q{sel(1)};
        p1 = Q{sel(2)};
        
        v0 = G.log(p0,p1);
        E_tmp = cdc_cost(G,Q,p0,p0,t);
        if (E_tmp < E)
            E = E_tmp;
            fprintf('Min. cost: %.3f\n', E);
            p0_opt = p0;
            v0_opt = v0;
        end
    end
end