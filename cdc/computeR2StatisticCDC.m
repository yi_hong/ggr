% R2 statstic based on CDC method

function R2 = computeR2StatisticCDC(p_opt, v_opt, G, data, time)

assert(length(time) == length(data));

dataEst = cell(length(data), 1);
for iI=1:length(time)
    dataEst{iI} = G.exp(p_opt, time(iI)*v_opt);
end

R2 = 1 - computeVar(dataEst, data, 3) / computeVar(data, data, 1);