% compute the average of the initial velocity for geodesic

function avgVel = averageVelocity( vel, t, tInit )

avgVel = zeros(size(vel{1}));
tDiffSum = 0.0;

for iI = 1:length(t)
    avgVel   = avgVel   + (t(iI) - tInit) * vel{iI};
    tDiffSum = tDiffSum + (t(iI) - tInit)^2;
end

avgVel = avgVel / tDiffSum;