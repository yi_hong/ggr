% compute the distances of measurements projected to the geodesic
% input:
% Y0: the initial point on the manifold
% Y0dot: the initial velocity in the tangent space
% t0: the associated value of the initial point
% oma: the measurements
% t: the sequence of the associated values
% tStep: the time step for integrating forward
% output:
% distSqrSum: the sum of square distances
% 
% Author: Yi Hong
% Date: 10/02/2013
%
function distSqrSum = sumDistOfPointsProjectToGeodesicODE45(Y0, Y0dot, t0, oma, t, distSumMin )

if nargin <= 5
    distSumMin = -1;
end

if t0 == min(t)
    tspan = linspace(t0, max(t), 50);
    [~, YForward] = integrateForwardWithODE45(Y0, Y0dot, tspan);
    YBackward{1} = Y0;
elseif t0 == max(t)
    tspan = linspace(t0, min(t), 50);
    [~, YBackward] = integrateForwardWithODE45(Y0, Y0dot, tspan);
    YForward{1} = Y0;
else
    tspan = linspace(t0, max(t), 50);
    [~, YForward] = integrateForwardWithODE45(Y0, Y0dot, tspan);
    tspan = linspace(t0, min(t), 50);
    [~, YBackward] = integrateForwardWithODE45(Y0, Y0dot, tspan);
end
        
% compute the sum of the square distances
distSqrSum = 0;
for id = 1:length(oma)   % for each measurement, find its projection
    distMin = -1;
    for iJ = 1:length(YForward)
        distTmp = grarc( oma{id}, YForward{iJ} );
        if distMin == -1 || distTmp < distMin
            distMin = distTmp;
        end
    end
    for iJ = 1:length(YBackward)
        distTmp = grarc( oma{id}, YBackward{iJ} );
        if distMin == -1 || distTmp < distMin
            distMin = distTmp;
        end
    end
    distSqrSum = distSqrSum + distMin;
    if distSumMin > 0 && distSumMin < distSqrSum
    	break;
    end
end
