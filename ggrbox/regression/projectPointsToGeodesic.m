% input:
% Y0: the initial point on the manifold
% Y0dot: the initial velocity in the tangent space
% t0: the associated value of the initial point
% oma: the measurements
% minT, maxT: the searching range
% tStep: the time step for integrating forward
% output:
% tEst: the estimated values
% 
% Author: Yi Hong
% Date: 10/02/2013
%
function tEst = projectPointsToGeodesic(Y0, Y0dot, t0, oma, minT, maxT, tStep )

% integrate forward from t0 to maxT
[YendForward] = integrateForwardToGivenTime(Y0, Y0dot, maxT-t0, tStep);
% integrate backward from t0 to minT
[YendBackward] = integrateForwardToGivenTime(Y0, Y0dot, minT-t0, tStep);

tEst = zeros( length(oma), 1 );
for iI = 1:length(oma)   % for each measurement, find its projection
    distMin = -1;
    tEst(iI) = NaN;
    for iJ = 1:length(YendForward)
        distTmp = grarc( oma{iI}, YendForward{iJ} );
        if iJ == 1 || distTmp < distMin
            distMin = distTmp;
            if iJ == length(YendForward)
                tEst(iI) = maxT;
            else
                tEst(iI) = (iJ-1)*tStep + t0;
            end
        end
    end

    for iJ = 1:length(YendBackward)
        distTmp = grarc( oma{iI}, YendBackward{iJ} );
        if distTmp < distMin
            distMin = distTmp;
            if iJ == length(YendBackward)
                tEst(iI) = minT; 
            else
                tEst(iI) = t0 - (iJ-1)*tStep;
            end
        end
    end
    
end