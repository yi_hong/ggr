% geodesic defined by two points
function [Y0, Y0dot, t0] = grpairSearch( oma, t, nStep )

strTmp = sprintf('Compute geodesic of %d objects using pair-search (geodesic defined by two points)', length(t));
disp(strTmp);

distMin = -1;
for iI = 1:length(t)
	for iJ = iI+1:length(t)
		if abs( t(iJ) - t(iI) ) < 1e-7
			continue;
		end
		[~, ~, ~, velInit] = grgeo( oma{iI}, oma{iJ}, 1, 'v3', 'v2' );
		% adjust the velocity to make it shooting from ti to tj
		velInit = velInit / (t(iJ) - t(iI));
        
       		 % compute the sum of the square distances
		distSum = sumDistOfPointsToGeodesic(oma{iI}, velInit, t(iI), oma, t, nStep );
		if distMin < 0 || distMin > distSum
            		distMin = distSum;
			Y0 = oma{iI};
			Y0dot = velInit;
			t0 = t(iI);
		end
	end
end


