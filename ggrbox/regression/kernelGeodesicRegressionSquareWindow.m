% local regression, with square window

function [errSqr, SAdapted] = kernelGeodesicRegressionSquareWindow(subSpace, xYear, xTest, nSize, grParams)

% subSpace: the points on the manifold
% xYear: the independent value
% xTest: current testing value
% nSize: the number of points will be used for regression

assert(length(xYear) > nSize);
grParams.nSys = nSize;

[~, indNear] = sort( abs(xYear - xTest) );
if xYear(indNear(1)) == xTest
    grParams.nSampling = sort(indNear(2:nSize+1));
    subSpaceTest = subSpace(indNear(1));
else
    grParams.nSampling = sort(indNear(1:nSize));
    subSpaceTest = [];
end
grParams.nTesting = grParams.nSampling;
xYear(grParams.nSampling)'

% regression
grParams.regressMethod = {'FullRegression'};
[~, ~, tPos, initPnt, initVel, ~, timeWarpParams] = estimateOnFeature( subSpace, xYear, grParams);

% compute testing error
[errSqr, SAdaptedTmp] = computeGeodesicTestError( initPnt{1}, initVel{1}, tPos{1}, ...
    timeWarpParams{1}, subSpaceTest, xTest );
if length(xTest) == 1
    SAdapted = SAdaptedTmp{1};
else
    SAdapted = SAdaptedTmp;
end