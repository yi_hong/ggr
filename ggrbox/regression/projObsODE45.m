% project a given Y to the geodeisc with its closest point
function [tOpt, YOpt] = projObs(Y, Y0, Ydot, t0, minT, maxT, nStep)

[YendForward] = integrateForwardToGivenTime(Y0, Ydot, maxT-t0, nStep);
[YendBackward] = integrateForwardToGivenTime(Y0, Ydot, minT-t0, nStep);

minDistForward = -1;
idOptForward = 0;
for iI = 1:length(YendForward)
    dist = grarc(Y, YendForward{iI});
    if minDistForward < 0 || minDistForward > dist
        idOptForward = iI;
        minDistForward = dist;
    end
end

minDistBackward = -1;
idOptBackward = 0;
for iI = 1:length(YendBackward)
    dist = grarc(Y, YendBackward{iI});
    if minDistBackward < 0 || minDistBackward > dist
        idOptBackward = iI;
        minDistBackward = dist;
    end
end

if minDistForward <= minDistBackward
    if idOptForward == length(YendForward)
        tOpt = maxT;
        YOpt = YendForward{end};
    else
        tOpt = t0 + (idOptForward-1)*nStep;
        YOpt = YendForward{idOptForward};
    end
else
    if idOptBackward == length(YendBackward)
        tOpt = minT;
        YOpt = YendBackward{end};
    else
        tOpt = t0 - (idOptBackward-1)*nStep;
        YOpt = YendBackward{idOptBackward};
    end
end

%{
minDist = -1;
for ti = minT:nStep:maxT
    [Yend] = integrateForwardToGivenTime( Y0, Ydot, ti-t0, 0.05 );
    dist = grarc( Y, Yend{end} );
    if minDist < 0 || minDist > dist
        minDist = dist;
        tOpt = ti;
        YOpt = Yend{end};
    end
end
%}