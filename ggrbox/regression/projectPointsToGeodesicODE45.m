% input:
% Y0: the initial point on the manifold
% Y0dot: the initial velocity in the tangent space
% t0: the associated value of the initial point
% oma: the measurements
% minT, maxT: the searching range
% tStep: the time step for integrating forward
% output:
% tEst: the estimated values
% 
% Author: Yi Hong
% Date: 10/02/2013
%
function [tEst, dist] = projectPointsToGeodesicODE45(Y0, Y0dot, t0, oma, minT, maxT, err)

assert(err > 0);   % the step for prediction is always greater than 0

if t0 <= minT
    tspan = t0:err:maxT;
    if tspan(end) < maxT
        tspan(end+1) = maxT;
    end
    [tForward, YForward] = integrateForwardWithODE45(Y0, Y0dot, tspan); %[t0 maxT]);
    % remove the part smaller than minT
    indexTmp = find(tForward < minT);
    tForward(indexTmp) = [];
    YForward(indexTmp) = [];
    tBackward(1) = t0;
    YBackward{1} = Y0;
elseif t0 >= maxT
    tspan = t0:-err:minT;
    if tspan(end) > minT
        tspan(end+1) = minT;
    end
    [tBackward, YBackward] = integrateForwardWithODE45(Y0, Y0dot, tspan); %[t0 minT]);
    % remove the part bigger than maxT
    indexTmp = find(tBackward > maxT);
    tBackward(indexTmp) = [];
    YBackward(indexTmp) = [];
    tForward(1) = t0;
    YForward{1} = Y0;
else
    
    if abs(t0 - maxT) < 1e-07
        tForward(1) = t0;
        YForward{1} = Y0;
    else
        tspan = t0:err:maxT;
        if tspan(end) < maxT
            tspan(end+1) = maxT;
        end
        [tForward, YForward] = integrateForwardWithODE45(Y0, Y0dot, tspan); %[t0 maxT]);
    end
    
    if abs(t0 - minT) < 1e-07
        tBackward(1) = t0;
        YBackward{1} = Y0;
    else
        tspan = t0:-err:minT;
        if tspan(end) > minT
            tspan(end+1) = minT;
        end
        [tBackward, YBackward] = integrateForwardWithODE45(Y0, Y0dot, tspan); %[t0 minT]);
    end
end
        
% compute the sum of the square distances
tEst = zeros( length(oma), 1 );
dist = zeros( length(oma), 1 );
% plot the searching distance
%figure, hold on;
distForward = zeros( length(YForward), 1);
distBackward = zeros( length(YBackward), 1 );
for id = 1:length(oma)
    for iJ = 1:length(YForward)
        distForward(iJ) = grarc( oma{id}, YForward{iJ} );
    end
    for iJ = 1:length(YBackward)
        distBackward(iJ) = grarc( oma{id}, YBackward{iJ} );
    end
    [minForward, idForward] = min(distForward);
    [minBackward, idBackward] = min(distBackward);
    if minForward <= minBackward
        tEst(id) = tForward(idForward);
        dist(id) = minForward;
        %plot(tEst(id), minForward, 'rp', 'MarkerSize', 5);
        %plot([tEst(id), tTest(id)], [minForward, minForward], 'm-.');
    else
        tEst(id) = tBackward(idBackward);
        dist(id) = minBackward;
        %plot(tEst(id), minBackward, 'rp', 'MarkerSize', 5);
        %plot([tEst(id), tTest(id)], [minBackward, minBackward], 'm-.');
    end
    %plot([flipud(tBackward(2:end)); tForward], [flipud(distBackward(2:end)); distForward], 'b-');
end
%hold off

%c = clock;
%filename = sprintf('./people_counting_results/projection/projectEnergyTest%f.png', c(end));
%saveas(gca, filename);
%close;

%{
for id = 1:length(oma)   % for each measurement, find its projection
    distMin = -1;
    for iJ = 1:length(YForward)
        distTmp = grarc( oma{id}, YForward{iJ} );
        if distMin == -1 || distTmp < distMin
            distMin = distTmp;
            tEst(id) = tForward(iJ);
        end
    end
    for iJ = 1:length(YBackward)
        distTmp = grarc( oma{id}, YBackward{iJ} );
        if distMin == -1 || distTmp < distMin
            distMin = distTmp;
            tEst(id) = tBackward(iJ);
        end
    end
end
%}
