%
% integrate forward in time for piecewise regression
%  
% Input: pntY   -- the initial point
%        tanYs  -- the initial velocity at the initial point and the breakpoints
%        params -- the parameters 
%  
% Output: X1    -- the points on the geodesic at each time step, nPieces * nPointsInEachPieces
%         X2    -- the corresponding initial velocity
%         Y_RK  -- the interpolated Y for backward computation
%
% Author: Yi Hong
% yihong@cs.unc.edu
%

function [X1, X2, Y_RK] = integrateForwardPiecewise( pntY, tanYs, params )
pntY_tmp = pntY;
for iI = 1:length(tanYs)
	[X1{iI}, X2{iI}, Y_RK{iI}] = integrateForwardMP( pntY_tmp, tanYs{iI}, params.Gns(iI), params.h );
	pntY_tmp = X1{iI}{end};
end
