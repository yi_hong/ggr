%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Compute the energy for continuous piecewise regression
%
%  Author: Yi Hong
%  Date: 10/13/2013
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [energyTotal, X1s] = computeEnergyPiecewise( X1, X2, params )


% compute the energy of the initial velocity
energyV = 0;
for iI = 1:length(X2)
	energyV = energyV + trace( (X2{iI}{1})' * X2{iI}{1} ) * params.Gns(iI) / params.nt;
end
energyV = energyV * params.alpha;

% compute the energy of the similarity term
energyS = 0;
for iI = 1:length(params.ts)
	% find the corresponding piece for the point iI	
	iJ = params.YgIds(iI);
	if iJ == 1
		X1s{iI} = X1{iJ}{params.ts(iI)};
	else
		X1s{iI} = X1{iJ}{params.ts(iI)-params.bks(iJ-1)+1};
	end

	energyS = energyS + grarc(X1s{iI}, params.Ys{iI}) * params.wi(iI);
end

energyS = energyS / params.sigmaSqr;

% compute the total energy
energyTotal = energyV + energyS;


