%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Line search update for continuous piecewise regression
%
%  Author: Yi Hong
%  yihong@cs.unc.edu
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [pntY, tanYs, currentEnergy, gradientBks, X1s] = lineSearchUpdatePiecewise(pntY0, tanY0, params)

% initial point
pntY = pntY0;

% initial velocities
if length(tanY0) == 1
    tanYs = cell(params.nPieces, 1);
    tanYs{1} = tanY0{1};
    % compute the initial velocity at each breakpoint
    Y0_tmp = pntY0;
    tanY0_tmp = tanY0{1};
    for iI = 1:params.nPieces-1
        [X1_tmp, X2_tmp] = integrateForwardMP( Y0_tmp, tanY0_tmp, params.Gns(iI), params.h );
        Y0_tmp = X1_tmp{end};
        tanY0_tmp = X2_tmp{end};
        tanYs{iI+1} = tanY0_tmp;
    end
else
    tanYs = tanY0;
end

% integrate forward
[X1, X2, Y_RK] = integrateForwardPiecewise( pntY, tanYs, params );

% compute energy
[currentEnergy, X1s] = computeEnergyPiecewise( X1, X2, params );
if ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint 
    fprintf( 'Continuous Piecewise GGR (CPGGR)\n' );
    fprintf( 'CPGGR Intial energy = %f\n', currentEnergy );
end

% plot the energy
figure(500), hold on;
clr = rand(1, 3);
eng_pre = currentEnergy;
figure(500), plot( 0, eng_pre, 'p', 'LineWidth', 4, 'Color', clr );
title('Continuous Piecewise Regression Energy');
xlabel('Iteration');
ylabel('Energy');

% the gradient for the breakpoints
gradientBks = [];
stopIteration = false;
for iI = 1:params.nIterMax
    % integrate backward
    [gradientX10, gradientX20s, lambdas] = integrateBackwardAndUpdateGradientPiecewise(X1, X2, X1s, Y_RK, params);
    
    % compute the gradient for the breakpoints
    for iJ = 1:length(lambdas)
        gradientBks(iJ) = trace( lambdas{iJ}' * (X2{iJ}{end} - X2{iJ+1}{1}) );
    end
    
    % time step size
    dt = params.deltaT;
    energyReduced = false;
    
    % store the current status
    curPntY0 = pntY;
	curTanYs = tanYs;
    
    % change the positive gradient to negative
    gradientX10 = -gradientX10;
    % make the gradientX10 to be tangent to the point
    gradientX10 = gradientX10 - curPntY0 * (curPntY0' * gradientX10);
    for iJ = 1:params.maxReductionSteps
		
		% update the initial point 
        % integrate forward
        if params.useODE45
            [~, Ytmp] = integrateForwardWithODE45( curPntY0, gradientX10, [0 dt/2 dt] );
        else
            [Ytmp] = integrateForwardToGivenTime( curPntY0, gradientX10, dt, params.h );
        end
        pntY = Ytmp{end};  % updated initial point
        
        if sum(sum(isnan(pntY))) > 0 
            if ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint 
                fprintf('#');
            end
            dt = dt * params.rho;
            continue; 
        end
        
		% update the initial velocity at the initial point 
		tanYs{1} = curTanYs{1} - dt * gradientX20s{1};
        % make sure the velocity is tangent to the point
        tanYs{1} = tanYs{1} - curPntY0 * (curPntY0' * tanYs{1});
        % parallel transport 
		tanYs{1} = ptEdelman(curPntY0, gradientX10, tanYs{1}, dt);
        % project to the new point
		tanYs{1} = tanYs{1} - pntY * (pntY' * tanYs{1});	

		% update the initial velocity at the breakpoints
        Y0_tmp = pntY;
		for iK = 2:length(curTanYs)
            % integrate forward 
            [X1_tmp, ~] = integrateForwardMP( Y0_tmp, tanYs{iK-1}, params.Gns(iK-1), params.h );
            
            % the initial point at the breakpoint
            Y0_tmp = X1_tmp{end};
            
            % update the initial velocity
			tanYs{iK} = curTanYs{iK} - dt * gradientX20s{iK};
            % make sure the velocity is tangent to the point
            tanYs{iK} = tanYs{iK} - X1{iK}{1} * ((X1{iK}{1})' * tanYs{iK});
            % paralle transport
            [~, ~, ~, vTmp] = grgeo(X1{iK}{1}, Y0_tmp, 1, 'v3', 'v2');
            tanYs{iK} = ptEdelman(X1{iK}{1}, vTmp, tanYs{iK}, 1);
            %tanYs{iK} = parallelTransport( tanYs{iK}, X1{iK}{1}, Y0_tmp, dt, params.useODE45 );
            % project to the breakpoint
			tanYs{iK} = tanYs{iK} - Y0_tmp * (Y0_tmp' * tanYs{iK});
        end
        
        % integrate forward in time
        [X1, X2, Y_RK] = integrateForwardPiecewise( pntY, tanYs, params );
        
        if sum(sum(isnan(X1{end}{end}))) + sum(sum(isnan(X2{end}{end}))) > 0 
            if ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint 
                fprintf('#');
            end
            dt = dt * params.rho;
            continue; 
        end

        % compute energy
        [energy, X1s] = computeEnergyPiecewise( X1, X2, params );
        %wolfeDecrement = 0; % params.c1 * dt * dot(pk, gk);
        
        if energy >= currentEnergy
            if ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint 
                fprintf('#');
            end
        else
            % energy decreased
            currentEnergy = energy;
            energyReduced = true;
            if ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint 
                fprintf( 'CPGGR Iter %04d: Energy = %f\n', iI, currentEnergy );
            end
            figure(500), plot( [iI-1 iI], [eng_pre currentEnergy], '-o', 'LineWidth', 2, 'Color', clr );
            drawnow
            
            if ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint 
                if isfield(params, 't_truth') && isfield(params, 'Y_truth') && isfield(params, 'pDim')
                    h = figure(100);
                    clf(h);
                    hold on
                    plotGeodesic( params.Y_truth, params.t_truth, params.pDim, '-' );
                    plotGeodesic( X1s, params.ts_pre, params.pDim, '--' );
                    drawnow
                    hold off
                end
            end
            
            if eng_pre - currentEnergy < params.stopThreshold
                stopIteration = true;
            end
            
            eng_pre = currentEnergy;
            
            break;
        end
        dt = dt * params.rho;
    end
    
    if (~energyReduced)
        if ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint 
            fprintf( 'CPGGR Iteration %d: Could not reduce energy further. Quitting.\n', iI );
        end
        pntY = curPntY0;
		tanYs = curTanYs;
        break;
    end
    
    if stopIteration
        if ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint
            fprintf( 'CPGGR Qutting because the decreased energy is smaller than the threshold.\n' );
        end
        break;
    end
end

    


