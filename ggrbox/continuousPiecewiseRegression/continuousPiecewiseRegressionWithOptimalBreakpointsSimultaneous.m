% 
% continuous piecewise regression with optimal breakpoints
% 
% Inputs: params (parameters for piecewise regression)
%         .Ys (measurements)
%         .ts (time points for each point)
%         .nBks (number of breakpoints)
% 
% Results: pntY  -- the initial point
%          tanYs -- a sequence of initial velocity at the 
%                   initial point and the breakpoints
%          bkPnts -- breakpoints
% 
% Author: Yi Hong
% yihong@cs.unc.edu
% 

function [pntY, tanYs, bkPnts, energy] = continuousPiecewiseRegressionWithOptimalBreakpointsSimultaneous( params )

% adjust the time points with the initial point
[params.ts, idSort] = sort( params.ts );
params.Ys = params.Ys(idSort);
params.wi = params.wi(idSort);

% initialize the breakpoints
if ~isfield(params, 'bks') || isempty(params.bks)
    tmp = linspace( params.ts(1), params.ts(end), params.nBks+2 );
    params.bks = tmp(2:end-1);
end

if isfield(params, 'Y0Init') && isfield(params, 'Y0dotInit') && length(params.Y0dotInit) == length(params.bks)+1   
    disp('Initialize with the given values');
    pntY0 = params.Y0Init;
    tanYs0 = params.Y0dotInit;
    for iI = 1:length(tanYs0)
        tanYs0{iI} = tanYs0{iI} * (max(params.ts) - min(params.ts));
    end
elseif isfield(params, 'pntY0') && isfield(params, 'tanY0s') && isfield(params, 'pntT0s') && ...
    length(params.tanY0s) == length(params.pntT0s) && sum(params.pntT0s(2:end) == params.bks) == length(params.bks)
    disp('Initialize with previous method');
    pntY0 = params.pntY0;
    tanYs0 = params.tanY0s;
    for iI = 1:length(tanYs0)
        tanYs0{iI} = tanYs0{iI} * (max(params.ts) - min(params.ts));
    end
else
    disp('Initialize with computing the continuous optimization');
    if ~( isfield(params, 'pntY0s') && isfield(params, 'tanY0s') && isfield(params, 'pntT0s') )
        [params.pntY0s, params.tanY0s, params.pntT0s] = piecewiseRegression(params);
    end
    [pntY0, tanYs0] = continuousPiecewiseRegression(params);
end

% scale the breakpoints to 0-1
bkPnts0 = params.bks;
for iI = 1:length(bkPnts0)
    bkPnts0(iI) = (bkPnts0(iI) - min(params.ts)) / (max(params.ts)-min(params.ts));
end

if strcmp( params.minFunc, 'linesearch' )
    [pntY, tanYs, bkPnts, energy] = lineSearchUpdatePiecewiseOB( pntY0, tanYs0, bkPnts0, params );
else
    if ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint
        error('Not supported minimization method');
    end
end

% scale the breakpoints back
for iI = 1:length(bkPnts)
    bkPnts(iI) = bkPnts(iI) * (max(params.ts) - min(params.ts)) + min(params.ts); 
end

end


