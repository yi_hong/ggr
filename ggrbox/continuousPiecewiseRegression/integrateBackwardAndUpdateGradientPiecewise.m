% 
% backward in time and update the gradient
% 
% Author: Yi Hong
% yihong@cs.unc.edu
% 


function [gradientX10, gradientX20s, lambdas] = integrateBackwardAndUpdateGradientPiecewise( X1, X2, X1s, Y_RK, params )

% backward in time
lam1_end = zeros( size(X1{1}{1}) );
lam2_end = zeros( size(lam1_end) );

% gradient for velocity
gradientX20s = cell(params.nPieces, 1);
% lambda for computing gradient for breakpoints
lambdas  = cell(params.nPieces-1, 1);
for iJ = params.nPieces:-1:1
    paramsTmp.nt = params.Gns(iJ);
    paramsTmp.h = params.h;
    paramsTmp.Ys = params.Ys(params.GyIds{iJ});
    paramsTmp.ts = params.ts(params.GyIds{iJ});
    if iJ > 1
        paramsTmp.ts = paramsTmp.ts - params.bks(iJ-1) + 1;
    end
    paramsTmp.sigmaSqr = params.sigmaSqr;
    paramsTmp.wi = params.wi(params.GyIds{iJ});
    [lam1, lam2] = integrateBackwardMP( lam1_end, lam2_end, Y_RK{iJ}, X1s(params.GyIds{iJ}), paramsTmp );
    % positive gradient for velocity
    gradientX20s{iJ} = 2*params.alpha*X2{iJ}{1} -lam2{end} + X1{iJ}{1} * ( (X1{iJ}{1})' * lam2{end} ); 
    if iJ > 1
        lam1_end = lam1{end} - X2{iJ}{1}*(lam2{end}'*X1{iJ}{1});
        lam2_end = zeros( size(lam1_end) );
        lambdas{iJ-1} = -lam1_end;
    end
end

% compute the gradient to update, positive gradient for point
gradientX10 = - lam1{end} + X1{1}{1} * ( (X1{1}{1})' * lam1{end} ) + X2{1}{1} * ( (lam2{end})' * X1{1}{1} );
