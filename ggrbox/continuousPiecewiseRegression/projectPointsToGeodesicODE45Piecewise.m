% input:
% Y0: the initial point on the manifold
% Y0dot: the initial velocity in the tangent space
% t0: the associated value of the initial point
% oma: the measurements
% minT, maxT: the searching range
% tStep: the time step for integrating forward
% output:
% tEst: the estimated values
% 
% Author: Yi Hong
% yihong@cs.unc.edu
%
function [tEst, dist] = projectPointsToGeodesicODE45Piecewise(Y0, Y0dots, t0s, oma, minT, maxT, err)

assert(err > 0);

if t0s(1) <= minT
    if maxT > max(t0s)
        t0s = [t0s maxT];
    end
    tForward = [];
    YForward = {};
    Y0_tmp = Y0;
    for iI = 1:length(t0s)-1
        tspan = t0s(iI):err:t0s(iI+1);
        if tspan(end) < t0s(iI+1)
            tspan(end+1) = t0s(iI+1);
        end
        [tForwardTmp, YForwardTmp] = integrateForwardWithODE45(Y0_tmp, Y0dots{iI}, tspan);
        Y0_tmp = YForwardTmp{end};
        if iI < length(t0s)-1
            tForward = [tForward; tForwardTmp(1:end-1)];
            YForward = [YForward; YForwardTmp(1:end-1)];
        else
            tForward = [tForward; tForwardTmp];
            YForward = [YForward; YForwardTmp];
        end
    end
    indexTmp = find( tForward < minT );
    tForward(indexTmp) = [];
    YForward(indexTmp) = [];
    indexTmp = find( tForward > maxT );
    tForward(indexTmp) = [];
    YForward(indexTmp) = [];
    tBackward(1) = t0s(1);
    YBackward{1} = Y0;
elseif t0s(1) >= maxT
    disp('There may be some problems because the initial velocity at the breakpoints won''t be used!');
    tspan = t0s(1):-err:minT;
    if tspan(end) > minT
        tspan(end+1) = minT;
    end
    [tBackward, YBackward] = integrateForwardWithODE45(Y0, Y0dots{1}, tspan); %[t0 minT]);
    indexTmp = find( tBackward > maxT );
    tBackward(indexTmp) = [];
    YBackward(indexTmp) = [];
    tForward(1) = t0s(1);
    YForward{1} = Y0;
else
    
    if maxT > max(t0s)
        t0s = [t0s maxT];
    end
    tForward = [];
    YForward = {};
    Y0_tmp = Y0;
    for iI = 1:length(t0s)-1
        tspan = t0s(iI):err:t0s(iI+1);
        if tspan(end) < t0s(iI+1)
            tspan(end+1) = t0s(iI+1);
        end
        [tForwardTmp, YForwardTmp] = integrateForwardWithODE45(Y0_tmp, Y0dots{iI}, tspan);
        Y0_tmp = YForwardTmp{end};
        if iI < length(t0s)-1
            tForward = [tForward; tForwardTmp(1:end-1)];
            YForward = [YForward; YForwardTmp(1:end-1)];
        else
            tForward = [tForward; tForwardTmp];
            YForward = [YForward; YForwardTmp];
        end
    end
    indexTmp = find( tForward > maxT );
    tForward(indexTmp) = [];
    YForward(indexTmp) = [];
    
    tspan = t0s(1):-err:minT;
    if tspan(end) > minT
        tspan(end+1) = minT;
    end
    [tBackward, YBackward] = integrateForwardWithODE45(Y0, Y0dots{1}, tspan); %[t0 minT]);
end
        
% compute the sum of the square distances
tEst = zeros( length(oma), 1 );
dist = zeros( length(oma), 1 );
distForward = zeros( length(YForward), 1);
distBackward = zeros( length(YBackward), 1 );
%figure, hold on
for id = 1:length(oma)
    %plot( tForward, distForward, 'b-' );
    %plot( tBackward, distBackward, 'g-' );
    for iJ = 1:length(YForward)
        distForward(iJ) = grarc( oma{id}, YForward{iJ} );
    end
    for iJ = 1:length(YBackward)
        distBackward(iJ) = grarc( oma{id}, YBackward{iJ} );
    end
    [minForward, idForward] = min(distForward);
    [minBackward, idBackward] = min(distBackward);
    if minForward <= minBackward
        tEst(id) = tForward(idForward);
        dist(id) = minForward;
    else
        tEst(id) = tBackward(idBackward);
        dist(id) = minBackward;
    end
    %plot( tEst(id), dist(id), 'rs' );
end
%hold off
