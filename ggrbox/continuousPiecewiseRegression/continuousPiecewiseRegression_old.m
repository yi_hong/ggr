% 
% continuous piecewise regression
% 
% Inputs: params (parameters for piecewise regression)
%         .Ys (measurements)
%         .ts (time points for each point)
% 
% Results: pntY  -- the initial point
%          tanYs -- a sequence of initial velocity at the 
%                   initial point and the breakpoints
% 
% Author: yihong
% yihong@cs.unc.edu
% 

function [pntY, tanYs, energy, gradientBks, pntYsGeo] = continuousPiecewiseRegression( params )

% adjust the time points with the initial point
[params.ts, idSort] = sort( params.ts );
params.Ys = params.Ys(idSort);
params.wi = params.wi(idSort);
params.bks = sort(params.bks);

% here, pntY0 and tanY0 are at t0
if isfield(params, 'Y0Init') && isfield(params, 'Y0dotInit') && length(params.Y0dotInit) == length(params.bks)+1
    % initialize with the groud-truth
    disp('Initialize with the groud-truth');
    pntY0 = params.Y0Init;
    for iI = 1:length(params.Y0dotInit)
        tanY0{iI} = params.Y0dotInit{iI} * (max(params.ts) - min(params.ts));
    end
elseif isfield( params, 'pntY0' ) && isfield( params, 'tanY0' ) && isfield( params, 'pntT0' )
    % initialize with the results from other methods, only for the initial
    % point and intial velocity, not for the breakpoints
    minT = min(params.ts);
    maxT = max(params.ts);
    pntY0 = params.pntY0;
    tanY0{1} = params.tanY0;
    if params.pntT0 > minT   % if the value of the given initial point is larger than the minimum value, move backward
        if params.useODE45
            [~, Ytmp, Ydottmp] = integrateForwardWithODE45( pntY0, -tanY0{1}, [0 (params.pntT0-minT)/2.0 params.pntT0-minT] );
        else
            [Ytmp, Ydottmp] = integrateForwardToGivenTime( pntY0, -tanY0{1}, params.pntT0-minT, (params.pntT0-minT)/50.0 );
        end
        pntY0 = Ytmp{end};
        tanY0{1} = -Ydottmp{end};
    elseif params.pntT0 < minT  % if the value is smaller than  the minimum value, move forward
        if params.useODE45
            [~, Ytmp, Ydottmp] = integrateForwardWithODE45( pntY0, tanY0{1}, [0 (minT-params.pntT0)/2.0 minT-params.pntT0] );
        else
            [Ytmp, Ydottmp] = integrateForwardToGivenTime( pntY0, tanY0{1}, minT-params.pntT0, (minT-params.pntT0)/50.0 );
        end
        pntY0 = Ytmp{end};
        tanY0{1} = Ydottmp{end};
    end
    tanY0{1} = tanY0{1} * (maxT-minT);  % scale the initial velocity to shoot from the minimum value to the maximum value in unit time
elseif isfield( params, 'pntY0s' ) && isfield( params, 'tanY0s' ) && isfield( params, 'pntT0s' )
    % initialize for initial points and breakpoints
    assert( length(params.pntY0s) == length(params.tanY0s) && length(params.tanY0s) == length(params.pntT0s) );
    if length(params.tanY0s) ~= length(params.bks)+1
        [pntY0, tanY0{1}] = initializeRegression( params.Ys, params.ts, params.alpha, params.sigmaSqr, params.useODE45, params.useRansac, params.wi );
    else
        [pntY0, tanY0] = removeJumpAtBreakpoint( params );
    end
else
	% not given, compute the initialization using piecewise regression
    %[pntY0, tanY0{1}] = initializeRegression( params.Ys, params.ts, params.alpha, params.sigmaSqr, params.useODE45, params.useRansac, params.wi );
    [params.pntY0s, params.tanY0s, params.pntT0s] = piecewiseRegression(params);
    [pntY0, tanY0] = removeJumpAtBreakpoint( params );
end

% convert value to id
params.ts_pre = params.ts;
params.bks = params.bks - params.ts(1);
params.ts = params.ts - params.ts(1);
params.bks = params.bks ./ params.ts(end);
params.ts = params.ts ./ params.ts(end);
params.ts = min( max( round(params.ts / params.h) + 1, 1 ), params.nt+1 );
params.bks = min( max( round(params.bks / params.h) + 1, 1 ), params.nt+1 );
params.nPieces = length(params.bks) + 1;

% divide the data into groups 
% for each point, find its belonging piece
params.YgIds = zeros(length(params.Ys), 1);
for iI = 1:length(params.Ys)
	iJ = 1;
	while iJ <= length(params.bks)
		if params.ts(iI) <= params.bks(iJ)
			break;
		end
		iJ = iJ + 1;
	end
	params.YgIds(iI) = iJ;
end

% for each piece, find its points
params.GyIds = cell(params.nPieces, 1);
for iJ = 1:length(params.GyIds)
	params.GyIds{iJ} = find( params.YgIds == iJ );
end
% for each piece, find its length
tmp = [1 params.bks params.nt+1];
params.Gns = tmp(2:end) - tmp(1:end-1);

if strcmp( params.minFunc, 'linesearch' )
    [pntY, tanYs, energy, gradientBks, pntYsGeo] = lineSearchUpdatePiecewise( pntY0, tanY0, params );
else
    error('Not supported minimization method');
end
end

    function [pntY0, tanY0] = removeJumpAtBreakpoint( params )
    minT = min(params.ts);
    maxT = max(params.ts);
    bks_tmp = [minT params.bks maxT];
    for iI = 1:length(params.tanY0s)
        pntY0s{iI} = params.pntY0s{iI};
        tanY0{iI} = params.tanY0s{iI};
        if params.pntT0s(iI) > bks_tmp(iI)
            if params.useODE45
                [~, Ytmp, Ydottmp] = integrateForwardWithODE45( params.pntY0s{iI}, -params.tanY0s{iI}, ...
                    [0 (params.pntT0s(iI)-bks_tmp(iI))/2.0 (params.pntT0s(iI)-bks_tmp(iI))] );
            else
                [Ytmp, Ydottmp] = integrateForwardToGivenTime( params.pntY0s{iI}, -params.tanY0s{iI}, ...
                    params.pntT0s(iI)-bks_tmp(iI), (params.pntT0s(iI)-bks_tmp(iI))/50.0 );
            end
            pntY0s{iI} = Ytmp{end};
            tanY0{iI} = -Ydottmp{end};
        elseif params.pntT0s(iI) < bks_tmp(iI)
            if params.useODE45
                [~, Ytmp, Ydottmp] = integrateForwardWithODE45( params.pntY0s{iI}, params.tanY0s{iI}, ...
                    [0 (bks_tmp(iI)-params.pntT0s(iI))/2.0 (bks_tmp(iI)-params.pntT0s(iI))] );
            else
                [Ytmp, Ydottmp] = integrateForwardToGivenTime( params.pntY0s{iI}, params.tanY0s{iI}, ...
                    bks_tmp(iI)-params.pntT0s(iI), (bks_tmp(iI)-params.pntT0s(iI))/50.0 );
            end
            pntY0s{iI} = Ytmp{end};
            tanY0{iI} = -Ydottmp{end};
        end
    end
    % transport the velocity
    pntY0_tmp = pntY0s{1};
    for iI = 1:length(tanY0)-1
        [~, Ytmp] = integrateForwardWithODE45( pntY0_tmp, tanY0{iI}, [bks_tmp(iI) (bks_tmp(iI)+bks_tmp(iI+1))/2 bks_tmp(iI+1)] );
        pntY0_tmp = Ytmp{end};
        %[~, ~, ~, Vtmp] = grgeo( pntY0s{iI+1}, pntY0_tmp, 1, 'v3', 'v2' );
        %tanY0{iI+1} = ptEdelman( pntY0s{iI+1}, Vtmp, tanY0{iI+1}, 1 );
        tanY0{iI+1} = parallelTransport(tanY0{iI+1}, pntY0s{iI+1}, pntY0_tmp, 0.001, true);
        tanY0{iI+1} = tanY0{iI+1} - pntY0_tmp * (pntY0_tmp' * tanY0{iI+1});
    end
    pntY0 = pntY0s{1};
    for iI = 1:length(tanY0)
        tanY0{iI} = tanY0{iI} * (maxT - minT);
    end
end

