%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Compute the energy
%
%  Author: Yi Hong
%  Date: 10/13/2013
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [energyTotal, X1s] = computeEnergy( X1, X2, params )


% compute the energy of the initial velocity
energyV = trace( (X2{1})' * X2{1} ) * params.alpha;

% compute the energy of the similarity term
energyS = 0;
X1s = cell(length(params.ts), 1);
for iI = 1:length(params.ts)
    X1s{iI} = X1{params.ts(iI)};
    energyS = energyS + grarc(X1s{iI}, params.Ys{iI}) * params.wi(iI);
end

%energyS = sumDistOfPointsToGeodesicODE45(X1{1}, X2{1}/(max(params.ts_pre)-min(params.ts_pre)), ...
%    params.ts_pre(1), params.Ys, params.ts_pre, -1, params.useODE45);

energyS = energyS / params.sigmaSqr;

% compute the total energy
energyTotal = energyV + energyS;


