% parallel transport T0 to T1 along the geodesic defined by I0 to I1
function V1 = parallelTransport( V0, I0, I1, dt, useODE45 )

[~, ~, ~, vTmp] = grgeo(I0, I1, 1, 'v3', 'v2');
if useODE45
    [~, iTmp] = integrateForwardWithODE45(I0, vTmp, [0 0.25 0.75 1.0]);
    I25 = iTmp{2};
    I75 = iTmp{3};
else
    [iTmp] = integrateForwardToGivenTime(I0, vTmp, 0.25, 0.02);
    I25 = iTmp{end};
    [iTmp] = integrateForwardToGivenTime(I0, vTmp, 0.75, 0.02);
    I75 = iTmp{end};
end

if useODE45
    [~, iTmp] = integrateForwardWithODE45(I0, V0, [0 dt/8 dt/4]);
else
    [iTmp] = integrateForwardToGivenTime(I0, V0, dt/4, 0.01);
end
[~, ~, ~, vTmp] = grgeo(iTmp{end}, I25, 1, 'v3', 'v2');

if useODE45
    [~, iTmp] = integrateForwardWithODE45(iTmp{end}, vTmp, [0 1.0 2.0]);
else
    [iTmp] = integrateForwardToGivenTime(iTmp{end}, vTmp, 2.0, 0.02);
end
[~, ~, ~, vTmp] = grgeo(iTmp{end}, I75, 1, 'v3', 'v2');

if useODE45
    [~, iTmp] = integrateForwardWithODE45(iTmp{end}, vTmp, [0 1.0 2.0]);
else
    [iTmp] = integrateForwardToGivenTime(iTmp{end}, vTmp, 2.0, 0.02);
end
[~, ~, ~, V1] = grgeo(I1, iTmp{end}, 1, 'v3', 'v2');
V1 = V1 / (dt/4);


