% compare initial conditions from different initialization
clear all
close all

nType = 1;    % 1:people, 2:traffic

switch nType
    case 1
        inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714_duke/people_FullGGR_FitAll';
        filenamePairwise = sprintf('%s/counting_RAW_pairwise_1000.mat', inputPrefix);
        filenameGiven = sprintf('%s/counting_RAW_given_200.mat', inputPrefix);
        tmpPairwise = load(filenamePairwise);
        tmpGiven = load(filenameGiven);
        minT = min(tmpPairwise.cntAvg);
        maxT = max(tmpPairwise.cntAvg);
    case 2
        inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714_duke/traffic_FullGGR_FitAll';
        filenamePairwise = sprintf('%s/traffic_RAW_pairwise_200.mat', inputPrefix);
        filenameGiven = sprintf('%s/traffic_RAW_given_1000.mat', inputPrefix);
        tmpPairwise = load(filenamePairwise);
        tmpGiven = load(filenameGiven);
        minT = min(tmpPairwise.speed);
        maxT = max(tmpPairwise.speed);
    otherwise
        Error('Not supported');
end

initPntPairwise = tmpPairwise.initPnt{1}{1};
initVelPairwise = tmpPairwise.initVel{1}{1};
t0Pairwise = tmpPairwise.tPos{1}(1);
pntForInitPairwise = tmpPairwise.pntForInit{1};
velForInitPairwise = tmpPairwise.velForInit{1};

% integrate forward and backward
if t0Pairwise == minT
    Ybegin = initPntPairwise;
else
    [tTmp, pntYs] = integrateForwardWithODE45(initPntPairwise, initVelPairwise, [t0Pairwise (t0Pairwise+minT)/2 minT]);
    Ybegin = pntYs{end};
end

if t0Pairwise == maxT
    Yend = initPntPairwise;
else
    [tTmp, pntYs] = integrateForwardWithODE45(initPntPairwise, initVelPairwise, [t0Pairwise (t0Pairwise+maxT)/2 maxT]);
    Yend = pntYs{end};
end

% compute the angles
[~,S,~] = svd(Ybegin'*Yend);
thetaPairwise = acos(diag(S));


initPntGiven = tmpGiven.initPnt{1}{1};
initVelGiven = tmpGiven.initVel{1}{1};
t0Given = tmpGiven.tPos{1}(1);
pntForInitGiven = tmpGiven.pntForInit{1};
velForInitGiven = tmpGiven.velForInit{1};

% integrate forward and backward
if t0Given == minT
    Ybegin = initPntGiven;
else
    [tTmp, pntYs] = integrateForwardWithODE45(initPntGiven, initVelGiven, [t0Given (t0Given+minT)/2 minT]);
    Ybegin = pntYs{end};
end

if t0Given == maxT
    Yend = initPntGiven;
else
    [tTmp, pntYs] = integrateForwardWithODE45(initPntGiven, initVelGiven, [t0Given (t0Given+maxT)/2 maxT]);
    Yend = pntYs{end};
end

% compute the angles
[~,S,~] = svd(Ybegin'*Yend);
thetaGiven = acos(diag(S));

% compute the middle point of two initial points, then transport their
% velocity for comparision
[~, ~, ~, vTmpPairwise] = grgeo(initPntPairwise, initPntGiven, 1, 'v3', 'v2');
[~, ~, ~, vTmpGiven] = grgeo(initPntGiven, initPntPairwise, 1, 'v3', 'v2');
initVelPairwisePT = ptEdelman(initPntPairwise, vTmpPairwise, initVelPairwise, 0.5);
initVelGivenPT = ptEdelman(initPntGiven, vTmpGiven, initVelGiven, 0.5);

fprintf('Geodeisc distance between initial points before optimization: %f\n', ...
    sqrt(grarc(pntForInitGiven, pntForInitPairwise)));
fprintf('Geodeisc distance between initial points after optimization: %f\n\n', ...
    sqrt(grarc(initPntGiven, initPntPairwise)));

fprintf('F-norm distance between initial velocities before optimization: %f\n', ...
    norm(velForInitGiven-velForInitPairwise, 'fro'));
fprintf('F-norm distance between initial velocities after optimization: %f\n', ...
    norm(initVelPairwise-initVelGiven, 'fro'));
fprintf('F-norm distance between transported initial velocities after optimization: %f\n\n', ...
    norm(initVelPairwisePT-initVelGivenPT, 'fro'));

fprintf('The angle between the starting and ending points with given initilization:\n');
disp(thetaGiven');
fprintf('The angle between the starting and ending points with pairwise searching for initilization:\n');
disp(thetaPairwise');