% 
% plot the 2D points on the geodesic
% Author: Yi Hong
% Email: yihong@cs.unc.edu
%

function generateImageAlongGeodesic( initPnt, initVel, tPos, timeWarpParams, tMinPlot, tMaxPlot, sigma, nNumShape, grParams, nTypeFlag )

assert( tMinPlot < tMaxPlot );
ages = linspace( tMinPlot, tMaxPlot, nNumShape );

% plot the points on the geodesic
%figure(400);
for iPlot = 1:length(initPnt)
    
    % need to check the part for piecewise regression because I'm now
    % focusing on the full GGR and time warped regression
    if strcmp( grParams.regressMethod{iPlot}, 'PiecewiseRegression' ) || ...
       strcmp( grParams.regressMethod{iPlot}, 'ContinuousPiecewiseRegression' ) || ...
       strcmp( grParams.regressMethod{iPlot}, 'ContinuousPiecewiseRegressionWithOptimalBreakpoints' )
        error('Not supported yet');
        
    elseif strcmp( grParams.regressMethod{iPlot}, 'timeWarpRegression' )
        % time points for shooting forward
        tSpan = sort(generalisedLogisticFunction( ages, timeWarpParams{iPlot} ));
        [t{iPlot}, pointsSyn] = computePointsAlongGeodesic( initPnt{iPlot}{1}, initVel{iPlot}{1}, tPos{iPlot}(1), tSpan );
        % go back to real time
        t{iPlot} = inverseLogisticFunction( t{iPlot}, timeWarpParams{iPlot} );
        
        % plot the optimized logistic function
        subplot(1, length(initPnt)+1, length(initPnt)+1), hold on;
        plot( ages, generalisedLogisticFunction(ages, timeWarpParams{iPlot}), 'bo-.', 'LineWidth', 2 );
        % plot the M point
        plot( timeWarpParams{iPlot}.M*[1 1], [0 generalisedLogisticFunction( timeWarpParams{iPlot}.M, ...
            timeWarpParams{iPlot} ) ], 'r--', 'LineWidth', 1 );
        hold off;
    else
        [t{iPlot}, pointsSyn] = computePointsAlongGeodesic( initPnt{iPlot}{1}, initVel{iPlot}{1}, tPos{iPlot}(1), ages );
    end

    clr = jet(tMaxPlot - tMinPlot + 1);

    %subplot(1, length(initPnt)+1, iPlot), hold on;
    pointsSynOrig = pointsSyn;
    
    if nTypeFlag == 1
        % connected all the points into a loop
        for iSyn = 1:length(pointsSyn)
            for iK = iSyn:length(pointsSyn)
            figure(iK), subplot(1, length(initPnt), iPlot), hold on;
            pointsSyn{iSyn} = pointsSynOrig{iSyn} * sigma;
            plot( [pointsSyn{iSyn}(:, 1); pointsSyn{iSyn}(1, 1)], -[pointsSyn{iSyn}(:, 2); ...
                pointsSyn{iSyn}(1, 2)], 'Color', clr(round(t{iPlot}(iSyn)-tMinPlot+1), :), 'LineWidth', 1 );
            end
        end
    elseif nTypeFlag == 2
        % simply plot all the points
        for iSyn = 1:length(pointsSyn)
            for iK = iSyn:length(pointsSyn)
            figure(iK), subplot(1, length(initPnt), iPlot), hold on;
            pointsSyn{iSyn} = pointsSynOrig{iSyn} * sigma;
            plot( pointsSyn{iSyn}(:, 1), pointsSyn{iSyn}(:, 2), '.', ...
                'Color', clr(round(t{iPlot}(iSyn)-tMinPlot+1), :), 'LineWidth', 1 );
            end
        end
    else
        error('Unknown plotting type');
    end
    
    hold off
    axis equal
    %colormap(clr);
    %caxis( [tMinPlot tMaxPlot] );
    %colorbar
end

outputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results/results_011714/rat_calivarium/figure';
for iI = 1:length(pointsSyn)
    %filename = sprintf( '%s/corpus_callosum%02d.png', outputPrefix, iI+72 );
    filename = sprintf( '%s/rat_calivarium%03d.png', outputPrefix, iI+144 );
    figure(iI);
    for iJ = 1:length(initPnt)
        subplot(1, length(initPnt), iJ);
        switch iJ
            case 1
                title('GGR');
            case 2
                title('Time-warped GGR (TW-GGR)');
        end
        axis equal;
        %xlim([-40 40]);
        %ylim([-25 25]);
        xlim([-700 600]);
        ylim([-500 500]);
    end
    set( gcf, 'PaperUnits', 'inches' );
    set( gcf, 'PaperSize', [16 9] );
    set( gcf, 'renderer', 'painters' );
    print( gcf, '-dpng', '-r600', filename );
    %saveas( gca, filename );
end

