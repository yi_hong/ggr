function plotEstimation( t, tOpt, idPerm, nTrain )
% 1-nTrain: the objects for training
% nTrain-end: the objects for testing
if nargin < 3
    idPerm = 1:length(t);
    nTrain = length(t);
end

hold on
plot( t(idPerm(1:nTrain)), tOpt(idPerm(1:nTrain)), 'r*', 'MarkerSize', 10 );
plot( t(idPerm(nTrain+1:end)), tOpt(idPerm(nTrain+1:end)), 'bo', 'MarkerSize', 10 );
plot( min(t):max(t), min(t):max(t), 'g-', 'LineWidth', 2 );
hold off
axis equal
minT = min( min(t), min(tOpt) );
maxT = max( max(t), max(tOpt) );
%xlim([minT maxT]);
%ylim([minT maxT]);
xlabel('Actual value');
ylabel('Estimated value');
