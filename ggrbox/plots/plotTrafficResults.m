close all
clear all

% HOOF results
tmpRAW = load('../results/traffic_new/traffic_RAW_state10_train12_ds0.30.mat');
speedRAW = tmpRAW.speed;
speedEstRAW = tmpRAW.speedEst;
MSERAW = mean( ( speedRAW - speedEstRAW{1} ).^2 );
ErrRAW = mean( abs( speedRAW - speedEstRAW{1} ) );

figure, hold on;
plot( speedRAW, 'k-', 'LineWidth', 2 );
plot( speedEstRAW{1}, 'r-.', 'LineWidth', 2 );
hold off
ylim([0 80]);
xlabel('Video number', 'FontSize', 24);
ylabel('Traffic speed (mile/hour)', 'FontSize', 24);
legend('Truth', 'Prediction');
grid on
set(gca, 'FontSize', 24);
