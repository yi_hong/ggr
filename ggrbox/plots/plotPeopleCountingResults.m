clear all
close all

% HOOF results
tmpHOOF = load('../results/people_new/hoof/counting_HOOF_start1_end4000_winsize400_winstep20_state10_dir1.mat');
cntAvgHOOF = tmpHOOF.cntAvg;
cntEstHOOF = tmpHOOF.cntEst;
MSEHOOF = mean( ( cntAvgHOOF - cntEstHOOF{1} ).^2 );
ErrHOOF = mean( abs( cntAvgHOOF - cntEstHOOF{1} ) );

figure, hold on;
plot( cntAvgHOOF, 'k-', 'LineWidth', 2 );
plot( cntEstHOOF{1}, 'r-.', 'LineWidth', 2 );
hold off
ylim([0 50]);
xlabel('Window number', 'FontSize', 24);
ylabel('Average count', 'FontSize', 24);
legend('Truth', 'Prediction');
grid on
set(gca, 'FontSize', 24);

% FEAT results
tmpFEAT = load('../results/people_new/all_feats/counting_FEAT_start1_end4000_winsize400_winstep20_state10_dir1.mat');
cntAvgFEAT_Dir1 = tmpFEAT.cntAvg;
cntEstFEAT_Dir1 = tmpFEAT.cntEst;
MSEFEAT_Dir1 = mean( (cntAvgFEAT_Dir1 - cntEstFEAT_Dir1{1} ).^2 );
ErrFEAT_Dir1 = mean( abs( cntAvgFEAT_Dir1 - cntEstFEAT_Dir1{1} ) );
tmpFEAT = load('../results/people_new/all_feats/counting_FEAT_start1_end4000_winsize400_winstep20_state10_dir2.mat');
cntAvgFEAT_Dir2 = tmpFEAT.cntAvg;
cntEstFEAT_Dir2 = tmpFEAT.cntEst;
MSEFEAT_Dir2 = mean( (cntAvgFEAT_Dir2 - cntEstFEAT_Dir2{1} ).^2 );
ErrFEAT_Dir2 = mean( abs( cntAvgFEAT_Dir2 - cntEstFEAT_Dir2{1} ) );

tmpFEAT = load('../results/people_new/all_feats/counting_FEAT_start1_end4000_winsize400_winstep20_state10_dir3.mat');
cntAvgFEAT_Dir3 = tmpFEAT.cntAvg;
cntEstFEAT_Dir3 = tmpFEAT.cntEst;
MSEFEAT_Dir3 = mean( (cntAvgFEAT_Dir3 - cntEstFEAT_Dir3{1} ).^2 );
ErrFEAT_Dir3 = mean( abs( cntAvgFEAT_Dir3 - cntEstFEAT_Dir3{1} ) );

figure, hold on;
subplot(3, 1, 1), hold on;
plot( cntAvgFEAT_Dir1, 'k-', 'LineWidth', 2 );
plot( cntEstFEAT_Dir1{1}, 'r-.', 'LineWidth', 2 );
ylim([0 50]);
xlabel('Window number', 'FontSize', 24);
ylabel('Average count', 'FontSize', 24);
grid on
set(gca, 'FontSize', 24);

subplot(3, 1, 2), hold on;
plot( cntAvgFEAT_Dir2, 'k-', 'LineWidth', 2 );
plot( cntEstFEAT_Dir2{1}, 'b-.', 'LineWidth', 2 );
ylim([0 50]);
xlabel('window number', 'FontSize', 24);
ylabel('average count', 'FontSize', 24);
grid on
set(gca, 'FontSize', 24);

subplot(3, 1, 3), hold on;
plot( cntAvgFEAT_Dir3, 'k-', 'LineWidth', 2 );
plot( cntEstFEAT_Dir3{1}, 'b-.', 'LineWidth', 2 );
ylim([0 50]);
xlabel('window number', 'FontSize', 24);
ylabel('average count', 'FontSize', 24);
grid on
set(gca, 'FontSize', 24);
