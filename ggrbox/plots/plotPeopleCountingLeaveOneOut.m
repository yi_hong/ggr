% plot the leave-one-out results of people counting 
clear all
close all

inputPrefix = '../results/people_final/raw';
namePrefix = 'counting_RAW_start1_end4000_winsize400_winstep400_state10_wsid0_alpha0.00_sigmasqr1.00_linesearch_weightedData0_ds0.10_dir1_Part';

nPart = 10;
nMethod = 3;
cntTest = zeros(nPart, nMethod, 2);
errTest = zeros(nPart, nMethod);
for iI = 1:nPart
    filename = sprintf('%s/%s%dOf%d.mat', inputPrefix, namePrefix, iI, nPart);
    tmp = load(filename);
    cntAvg = tmp.cntAvg;
    cntEst = tmp.cntEst;
    nTest = tmp.grParams.nTesting;
    for iJ = 1:length(cntEst)
        cntTest(iI, iJ, 1) = cntAvg(nTest);
        cntTest(iI, iJ, 2) = cntEst{iJ}(nTest);
        errTest(iI, iJ) = abs( cntTest(iI, iJ, 1) - cntTest(iI, iJ, 2) );
    end
end

maxValue = max(max(max(cntTest)));
minValue = min(min(min(cntTest)));

for iJ = 1:nMethod
    subplot(1, nMethod+1, iJ), hold on;
    plot( cntTest(:, iJ, 1), cntTest(:, iJ, 2), 'bo', 'MarkerSize', 10 );
    plot( minValue:maxValue, minValue:maxValue, 'm--', 'LineWidth', 1 );
    axis equal
    xlim([15 40]);
    ylim([15 40]);
end
subplot(1, nMethod+1, nMethod+1), boxplot( errTest );
