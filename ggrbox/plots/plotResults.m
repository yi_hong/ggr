% plot the results
close all
clear all

nType = 1;  % 1: people, 2: traffic, 3: amy
useWeightedStd = true;

if nType == 1
    
    inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714_duke/people_3Ms_200_300';
    namePrefix = 'counting_RAW_start1_end4000_winsize400_winstep100_state10_wsid1_sigma100_alpha0.00_sigmasqr1.00_linesearch_weightedData0_dir1_Init0_ds0.25_Part';
    %namePrefix = 'counting_DICT_start1_end1320_winsize130_winstep30_state10_wsid1_sigma30_alpha0.00_sigmasqr1.00_linesearch_weightedData1_dir1_Init0_avg_row_code_5_3_Part';
    haveStdRegion = true;
    
    nWinStep = 100;
    nWinSize = 400;
    minValue = 15;
    maxValue = 40;
    nPartition = 4;
    nTotalNum = round((4000-nWinSize)/nWinStep) + 1;
    
    %tmp = load( '../data/ucsdpeds_feats/features/Peds1_feats.mat' );  % 1-4000 frames
    tmp = load( '/Users/yihong/Desktop/work/Project/GGR_data_results/data/ucsdpeds_feats/features/Peds1_feats.mat' );
	% using raw imaging, can only count the total number of people
	counts = tmp.cnt{end};
    truth = zeros( nTotalNum, 2 );   % 1st column: true values, 2st column: standard deviations
    
    nFrameStart = 1;
    for iI = 1:nTotalNum
        idStart = (iI-1)*nWinStep + nFrameStart;
        idEnd = (iI-1)*nWinStep + nFrameStart + nWinSize - 1;
        
        if ~useWeightedStd
            truth(iI, 1) = mean(counts(idStart:idEnd));
            truth(iI, 2) = std(counts(idStart:idEnd));
        else
            weights = gaussmf( 1:nWinSize, [100 round(nWinSize/2)] );
            weights = weights ./ sum(weights);
            mNum = length( find( weights ~= 0 ) );
            truth(iI, 1) = sum( counts(idStart:idEnd) .* weights );
            truth(iI, 2) = sqrt( sum( weights .* ( counts(idStart:idEnd) - truth(iI, 1) ).^2 ) * mNum / (mNum-1) );
        end
    end

elseif nType == 2
        
    %inputPrefix = '../results/traffic_final/raw_newPT/unsort';
    %inputPrefix = '../results/traffic_final/raw_newPT/sort';
    %inputPrefix = '../results/traffic_final/raw_newPT/sort/sort_1200';
    %namePrefix1 = 'traffic_RAW_state10_train202_ds1.00_wsi0_linesearch_weightData0__Part';
    %namePrefix2 = 'traffic_RAW_state10_train203_ds1.00_wsi0_linesearch_weightData0__Part';
    
    inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714_duke/traffic_3Ms_100_300';
    namePrefix1 = 'traffic_RAW_state10_train202_ds1.00_wsi0_linesearch_weightData0_Part';
    namePrefix2 = 'traffic_RAW_state10_train203_ds1.00_wsi0_linesearch_weightData0_Part'; 
    
    minValue = 10;
    maxValue = 70;
    nPartition = 5;
    nTotalNum = 253;
    
elseif nType == 3
    inputPrefix = '../results/amy';
    %namePrefix = 'amy_Part';
    namePrefix = 'amy_wd0_Part';
    minValue = 0;
    maxValue = 300000;
    nPartition = 12;
    nTotalNum = 12;
end


fz = 10;
fz2 = 20;

regressMethod = {'Pairwise Searching', 'Full GGR', 'Piecewise Full GGR'}; % 'Continuous P-F-GGR', 'Continuous P-F-GGR-OB'};

nRows = nPartition + 1;
nCols = 3;
maxEstErr = 0;

% 1-3: training, 4-6: testing
absErr2 = zeros( nPartition, length(regressMethod), 6 );
predictions = zeros( nTotalNum, length(regressMethod)); % 2: predictions

for iI = 1:nPartition
    if nType == 1 || nType == 3
        %filename = sprintf('%s/%s%d.mat', inputPrefix, namePrefix, iI);
        filename = sprintf('%s/%s%dOf%d.mat', inputPrefix, namePrefix, iI, nPartition);
    else
        if iI <= 3
            filename = sprintf('%s/%s%dOf%d.mat', inputPrefix, namePrefix1, iI, nPartition);
        else
            filename = sprintf('%s/%s%dOf%d.mat', inputPrefix, namePrefix2, iI, nPartition);
        end
    end
    tmp = load(filename);
    
    if nType == 1
        cntAvg = tmp.cntAvg;
        cntEst = tmp.cntEst;
    elseif nType == 2
        cntAvg = tmp.speed;
        cntEst = tmp.speedEst;
    elseif nType == 3
        cntAvg = tmp.fibro;
        cntEst = tmp.fibroEst;
    end
    nTraining = tmp.grParams.nSampling;
    nTesting = tmp.grParams.nTesting;
    
    if (nType == 1 && ~haveStdRegion) || nType == 2 || nType == 3
        %clear truth
    	truth = cntAvg;
    end

    for iJ = 1:length(cntEst)
        
        predictions(nTesting, iJ) = cntEst{iJ}(nTesting);
             
        absErr2(iI, iJ, 1) = sum( abs( cntEst{iJ}(nTraining) - cntAvg(nTraining) ) );
        absErr2(iI, iJ, 2) = length(nTraining);
        absErr2(iI, iJ, 3) = absErr2(iI, iJ, 1) ./ absErr2(iI, iJ, 2);
        
        absErr2(iI, iJ, 4) = sum( abs( cntEst{iJ}(nTesting) - cntAvg(nTesting) ) );
        absErr2(iI, iJ, 5) = length(nTesting);
        absErr2(iI, iJ, 6) = absErr2(iI, iJ, 4) ./ absErr2(iI, iJ, 5);
    end
end

absMeanErr = zeros( size(absErr2, 2), 2 );
stdErr = zeros( size(absErr2, 2), 2 );
for iJ = 1:length(absMeanErr)
    absMeanErr(iJ, 1) = sum( absErr2(:, iJ, 1) ) ./ sum( absErr2(:, iJ, 2) );
    stdErr(iJ, 1) = std( absErr2(:, iJ, 3) );
    
    absMeanErr(iJ, 2) = sum( absErr2(:, iJ, 4) ) ./ sum( absErr2(:, iJ, 5) );
    stdErr(iJ, 2) = std( absErr2(:, iJ, 6) );
end

figure, barwitherr( stdErr, absMeanErr, 'BarWidth', 0.8);
%barmap = [0.7 0.7 0.7; 0.05 0.45 0.1];
barmap = [0.3 0.3 1.0; 1.0 0.3 0.3];
colormap(barmap);
legend('Train', 'Test');
xticklabel_rotate(1:length(regressMethod), 45, regressMethod);
box off
grid on
set(gca, 'FontSize', fz2);

% plot truth-predction
figure, hold on;
for iJ = 1:size(predictions, 2)
    subplot(1, size(predictions, 2), iJ), hold on;
    plot( truth(:, 1), predictions(:, iJ), 'b*' );
    plot( minValue:maxValue, minValue:maxValue, 'm--' );
    % least sqaure fit
    pValue = polyfit( truth(:, 1), predictions(:, iJ), 1);
    xTmp = 0:max(truth(:, 1));
    plot( xTmp, pValue(1)*xTmp+pValue(2), 'k--' );
    axis equal
    xlim([minValue maxValue]);
    ylim([minValue maxValue])
    grid on
    xlabel('True values');
    ylabel('Predictions');
end

% plot curves
figure, hold on;
if nType == 1
    for iJ = 1:size(predictions, 2)
        subplot(1, size(predictions, 2), iJ), hold on;
        if haveStdRegion
            h=fill( [1:size(truth, 1), fliplr(1:size(truth,1))], ...
                  [(truth(:, 1)-truth(:, 2))', fliplr((truth(:, 1)+truth(:, 2))')], [0.85 0.85 0.85]);
            set(h, 'EdgeColor', 'None');
        end
    end
elseif nType == 2
    [truth(:, 1), idSort] = sort(truth(:, 1));
    for iJ = 1:size(predictions, 2)
        predictions(:, iJ) = predictions(idSort, iJ);
    end
end

style = {'go', 'bs', 'm*', 'k.', 'cp'};
for iJ = 1:size(predictions, 2)
    subplot(1, size(predictions, 2), iJ), hold on;
    plot( predictions(:, iJ), style{iJ}, 'LineWidth', 2);
    plot( truth(:, 1), 'r-.', 'LineWidth', 2 );
end
hold off
%saveas(gca, 'prediction_truth_max_pooling_crowd.fig');

%if nType == 1
%    save('crowd_count_prediction_wsid_wd.mat', 'truth', 'predictions', 'absMeanErr', 'stdErr');
%elseif nType == 2
%    save('traffic_speed_prediction.mat', 'truth', 'predictions', 'absMeanErr', 'stdErr');
%end
