%
% generate colormap
% Author: Yi Hong
% Email: yihong@cs.unc.edu

function cmap = interpolateCMap( colorStart, colorMid, colorEnd, numPart1, numPart2)

cmap = zeros( numPart1+numPart2, 3 );
for iI = 1:3
    cmap(1:numPart1, iI) = linspace(colorStart(iI), colorMid(iI), numPart1);
    tmp = linspace(colorMid(iI), colorEnd(iI), numPart2+1);
    cmap(numPart1+1:numPart1+numPart2, iI) = tmp(2:end);
end