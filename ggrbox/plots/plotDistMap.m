%compute the distance between points
function distMap = plotDistMap(subSpace, x)

[xSort, indSort] = sort(x);
subSpaceSort = subSpace(indSort);

distMap = zeros(length(subSpaceSort), length(subSpaceSort));
for iI = 1:size(distMap,1)
    for iJ = 1:size(distMap,2)
        distMap(iI, iJ) = grarc(subSpaceSort{iI}, subSpaceSort{iJ});
    end
    distMap(iI, iI) = NaN;
end

imagesc(distMap);
%xTmp = linspace(xSort(1), xSort(end), 6);
set(gca, 'XTickLabel', xSort);
set(gca, 'YTickLabel', xSort);