function plotSig( A, B, str, CC )

A1 = CC' * A;
B1 = CC' * B;
figure;
plot( A1(1, :), 'r-' );
hold on
plot( B1(1, :), 'b--' );
hold off
legend('Original', 'Synthetic');
ylim([-1, 1]);

%{
for iI = 1:size(A, 2)
    clf;
    plot( A1(1, iI), A1(2, iI), 'r-' );
    hold on
    plot( B1(1, iI), B1(2, iI), 'b--' );
    legend('Original', 'Synthetic');
    %ylim( [min(min(A1(1, :)), max(max(A1(2, :)))] );
    title(str);
    drawnow;
end
%}