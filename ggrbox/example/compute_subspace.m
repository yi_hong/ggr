function res = compute_subspace(D,dim,lam,seed)
    % set RNG seed
    if (seed>0)
        rng(seed);
    end

    t0 = tic;
    
    fts = D;
    N = size(fts,1);
    
    % normalize
    %fts = fts ./ repmat(sum(fts,2),1,size(fts,2));
    %tmp_Z = zscore(fts,1);
    tmp_Z = fts - repmat(mean(fts), [N 1]);
    
    % only use alpha*N samples for PCA
    tmp_Zhat = tmp_Z;
    if (lam > 0)
        fprintf('using %d samples ...\n', round(N*lam));
        tmp_Zhat = tmp_Z(randsample(1:N,round(N*lam)),:);
    end
    
    % run PCA
    [tmp_P,tmp_S,tmp_L] = princomp(tmp_Zhat);
    
    elapsed = toc(t0);
    fprintf('[compute_subspace]: took %.3g [sec]\n', elapsed);
    
    res.Z = tmp_Z;
    res.P = tmp_P(:,1:dim);
    res.S = tmp_S(:,1:dim);
    res.L = tmp_L;
    res.M = mean(fts);   % save the mean
    
    %cumsum(res.L(1:dim)) / sum(res.L);
    
    % quick sanity check - PROJECTION
    %assert(norm(tmp_S(1,:)' - tmp_P'*tmp_Z(1,:)') < 1e-6, ...
    %    'Projection gone wrong!');
end






