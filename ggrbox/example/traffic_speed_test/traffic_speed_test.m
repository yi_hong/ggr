% traffic data

close all
clear all

%profile -memory on

grParams.inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results';
outputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results/traffic';

%grParams.inputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results';
%outputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results/results_011714/traffic_3Ms';

grParams.sysid = 'dt';         % dynamical texture
grParams.useWeightedSysid = false;
grParams.featureName = 'RAW';  % RAW, RAW_ORG, HOOF
%grParams.regressMethod = {'PairSearch', 'FullRegression', 'PiecewiseRegression', 'ContinuousPiecewiseRegression', 'ContinuousPiecewiseRegressionWithOptimalBreakpoints'}; 
grParams.regressMethod = {'PairSearch', 'FullRegression', 'PiecewiseRegression'}; 
grParams.minValue = 0;
grParams.maxValue = 80;

grParams.sigmaSqr = 1;
grParams.alpha = 0.0;
grParams.nt = 200;
grParams.deltaT = 0.01;
grParams.deltaTBk = 0.05;
grParams.maxReductionSteps = 10;
grParams.rho = 0.5;
grParams.nIterMax = 1000;
grParams.nIterMaxBk = 50;
grParams.stopThreshold = 1e-6;
grParams.useODE45 = true;
grParams.minFunc = 'linesearch';
grParams.estErr = 0.05;
grParams.bks = [50];
grParams.breakpoint = [grParams.minValue, grParams.bks, grParams.maxValue];
grParams.useWeightedData = false;
grParams.useRansac = true;
grParams.useFullInitializePiecewise = true;
grParams.useFullInitializeContinuous = false;
grParams.usePiecewiseInitializeContinuous = true;
grParams.useContinuousInitializeContinuousOB = true; % use CPGGR to initialize the CPGGR with optimal breakpoint

grParams.optimizeBreakpoint = 2; % 1: update initial conditions and breakpoints simultaneously, 2: iteratively 

grParams.downsampling = 1.0;
grParams.nSta = 10;
grParams.nObs = grParams.nSta;

% load feature and do sid
[oma, speed] = loadTrafficFeatureAndSysId( grParams );
grParams.nSys = length(oma);

nPartition = 5;
for iP = 1:nPartition
    grParams.nTesting = iP:nPartition:grParams.nSys;
    grParams.nSampling = 1:grParams.nSys;
    grParams.nSampling(grParams.nTesting) = [];

    % training and testing
    [speedEst, errEst, tPos, initPnt, initVel, totEnergy] = estimateOnFeature( oma, speed, grParams, iP );
    
    % save the plot
    filename = sprintf('%s/traffic_%s_state%d_train%d_ds%.2f_wsi%d_%s_weightData%d_Part%dOf%d.fig', ...
        outputPrefix, grParams.featureName, grParams.nSta, length(grParams.nSampling), ...
        grParams.downsampling, grParams.useWeightedSysid, grParams.minFunc, grParams.useWeightedData, iP, nPartition);
    saveas( gca, filename );
    filename = sprintf('%s/traffic_%s_state%d_train%d_ds%.2f_wsi%d_%s_weightData%d_Part%dOf%d.png', ...
        outputPrefix, grParams.featureName, grParams.nSta, length(grParams.nSampling), ...
        grParams.downsampling, grParams.useWeightedSysid, grParams.minFunc, grParams.useWeightedData, iP, nPartition);
    saveas( gca, filename );

    % save results
    filename = sprintf('%s/traffic_%s_state%d_train%d_ds%.2f_wsi%d_%s_weightData%d_Part%dOf%d.mat', ...
        outputPrefix, grParams.featureName, grParams.nSta, length(grParams.nSampling), ...
        grParams.downsampling, grParams.useWeightedSysid, grParams.minFunc, grParams.useWeightedData, iP, nPartition);
    save(filename, 'speed', 'speedEst', 'grParams', 'errEst', 'tPos', 'initPnt', 'initVel', 'totEnergy');
end

h = figure(1000);
filename = sprintf('%s/traffic_optimal_breakpoints.fig', outputPrefix);
saveas(h, filename);

%p = profile('info');
%filename = sprintf('%s/traffic_speed_profile', outputPrefix);
%profsave(p, filename);
