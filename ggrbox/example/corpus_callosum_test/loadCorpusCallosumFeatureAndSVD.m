% load the landmarks for corpus callosum
% apply svd on the landmarks to convert them to Grassmannian

function [omas, ages, sigma] = loadCorpusCallosumFeatureAndSVD( grParams )
    [landmarks, ages] = readLandmarksAndAges( grParams );
    omas = cell( length(landmarks), 1 );
    sigma = zeros(size(landmarks{1}, 2), size(landmarks{1}, 2));
    sigmaPlot = zeros(length(landmarks), 2);
    for iI = 1:length(landmarks)
        landmarks{iI} = landmarks{iI} - repmat( mean(landmarks{iI}, 1), size(landmarks{iI}, 1), 1 );
        [omas{iI}, tmp, ~] = svd( landmarks{iI}, 0 );
        sigma = sigma + tmp;
        sigmaPlot(iI, 1) = tmp(1, 1);
        sigmaPlot(iI, 2) = tmp(2, 2);
    end
    sigma = sigma ./ length(landmarks);
    figure(403), boxplot(sigmaPlot);
end

function [landmarks, ages] = readLandmarksAndAges( grParams )

% corpus callosum

n = 32;   % the number of corpus shapes
landmarks = cell(n, 1);
for iI = 1:n
    filename = sprintf( '%s/data/Corpus_Shape/cc.%02d.wpts', grParams.inputPrefix, iI-1 );
    fid = fopen( filename, 'r' );
    tline = fgets(fid);
    clear points
    iJ = 0;
    while ~feof(fid)
        tline = fgets(fid);
        idPos = sscanf( tline, ' "%d" ' );
        iJ = iJ + 1;
        points(iJ, : ) = sscanf( tline, ' %*s %f %f ' );
    end 
    fclose( fid );
    landmarks{iI} = points;
end
% the age for each corpus
filename = sprintf('%s/data/Corpus_Shape/regressionparams.txt', grParams.inputPrefix );
fid = fopen( filename, 'r' );
ages = zeros( n, 1 );
nCountTmp = 0;
while ~feof(fid)
    tline = fgets(fid);
    nCountTmp = nCountTmp + 1;
    ages(nCountTmp) = sscanf( tline, '%d' );
end
fclose( fid );
end
