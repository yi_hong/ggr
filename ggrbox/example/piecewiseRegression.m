function [Y0, Y0dot, t0] = piecewiseRegression(grParams)
    grParams.breakpoint = [min(grParams.ts), grParams.bks, max(grParams.ts)];
    nPieces = length(grParams.breakpoint) - 1;
    iI = 1;
    while iI <= nPieces
        idPoints = find( grParams.ts  >= grParams.breakpoint(iI) & grParams.ts <= grParams.breakpoint(iI+1) );
        if length(idPoints) < 2
            grParams.breakpoint(iI+1) = [];
            nPieces = nPieces - 1;
            continue;
        end
        paramsRegress = grParams;
        paramsRegress.Ys = grParams.Ys(idPoints);
        paramsRegress.ts = grParams.ts(idPoints);
        paramsRegress.wi = grParams.wi(idPoints);
        paramsRegress.wi = paramsRegress.wi ./ sum(paramsRegress.wi) * length(paramsRegress.wi);
        
        [Y0{iI}, Y0dot{iI}] = fullRegressionOnGrassmannManifold( paramsRegress );
        Y0dot{iI} = Y0dot{iI} ./ ( max(paramsRegress.ts) - min(paramsRegress.ts) );
        t0(iI) = min(paramsRegress.ts);
        iI = iI + 1;
    end
end
