function omaEst = extendFromTwoPoints( omaStart, omaEnd, xYearStart, xYearEnd, xYearEst )

if abs(xYearEst - xYearStart) < 1e-7
    omaEst = omaStart;
    return;
end

%if abs(xYearEst - xYearEnd) < 1e-7
%    omaEst = omaEnd;
%    return;
%end

[~, ~, ~, velInit] = grgeo( omaStart, omaEnd, 1, 'v3', 'v2' );
assert(abs(xYearStart - xYearEnd) > 1e-7);
velInit = velInit / (xYearEnd - xYearStart);

[~, omaTmp] = integrateForwardWithODE45( omaStart, velInit, [xYearStart (xYearStart+xYearEst)/2 xYearEst] );
omaEst = omaTmp{end};