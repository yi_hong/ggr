% people counting test example

close all
clear all

%profile -memory on

%grParams.inputPrefix = 'Users/yihong/Desktop/work/Project/GGR_data_results';
%outputPrefix = 'Users/yihong/Desktop/work/Project/GGR_data_results/results/people';

grParams.inputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results';         % data for regression
outputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results/results_011714/people_dict_3Ms';  % regression results

grParams.sysid = 'dt';                % n4sid or dt
grParams.useWeightedSysid = true;     % use weighted system identification
grParams.sigmaWeightedSysid = 100;    % the bandwidth for weighted system identification
grParams.featureName = 'RAW';         % featureName = {'RAW', 'FEAT', 'HOOF', 'DICT'};
%grParams.poolDirection = 'row';       % the direction for pooling, row, col
%grParams.poolStyle = 'var';           % the way for pooling, max, min, minmax, avg, var
%grParams.sparseCode = 'code_spams';             % code_spams 
%grParams.regressMethod = {'PairSearch', 'FullRegression', 'PiecewiseRegression', 'ContinuousPiecewiseRegression', 'ContinuousPiecewiseRegressionWithOptimalBreakpoints'};  
grParams.regressMethod = {'PairSearch', 'FullRegression', 'PiecewiseRegression'};  
grParams.minValue = 0;                % the minimal range of the independent value, for plots                
grParams.maxValue = 50;               % the maximal range of the independent value, for plots

% parameters for compute the cost function
grParams.sigmaSqr = 1;                             % the coefficient for matching term in the cost function
grParams.alpha = 0;                                % the coefficient for prior knowledge
grParams.nt = 100;
grParams.deltaT = 0.1;
grParams.deltaTBk = 0.5;
grParams.maxReductionSteps = 10;
grParams.rho = 0.5;
grParams.nIterMax = 300;
grParams.nIterMaxBk = 50;
grParams.stopThreshold = 1e-6;
grParams.minFunc = 'linesearch';                    % linesearch, fmincon
grParams.useODE45 = true;                           % more stable
grParams.estErr = 0.05;                             % discretizing size for prediction
grParams.bks = [23];                                % breakpoints
grParams.breakpoint = [grParams.minValue, grParams.bks, grParams.maxValue];  % breakpoint with two boundaries
grParams.useWeightedData = true;                    % weight data using its density
grParams.useRansac = false;                         % randomly choose pairs for pair-wise searching
grParams.useFullInitializePiecewise = false;        % use full GGR results to initialize the piecewise GGR
grParams.useFullInitializeContinuous = false;       % use full GGR results to initialize the continuous piecewise GGR
grParams.usePiecewiseInitializeContinuous = true;   % use piecewise GGR results to initialize the continuous piecewise GGR
grParams.useContinuousInitializeContinuousOB = true; % use CPGGR to initialize the CPGGR with optimal breakpoint
grParams.optimizeBreakpoint = 2;                     % 1: update initial conditions and breakpoints simultaneously, 2: iteratively

if strcmp( grParams.featureName, 'FEAT' )
    grParams.allFEATS = 1;  % this parameter is to control how many FEATs used, no effect on RAW and HOOF
    if grParams.allFEATS == 0
        disp( 'Use part of the features and subtract mean in the sid' );
    else
        disp( 'Use all of the features and subtract mean in the sid' );
    end
end

grParams.downsampling = 0.25;        % downsample the raw image to reduce the dimension of the matrix
grParams.nFrameStart = 1;            % starting frame
grParams.nFrameEnd = 4000;           % ending frame
grParams.nWinSize = 400;             % video clip, including 400 frames
grParams.nWinStep = 100;             % moving step, 100 frames
grParams.nSta = 10;                  % the number of the state
grParams.nObs = grParams.nSta;       % the number k of the observation matrix
grParams.nSys = floor( (grParams.nFrameEnd - grParams.nFrameStart + 1 - ...
    grParams.nWinSize) / grParams.nWinStep ) + 1;    % the number of the observations

if ~strcmp(grParams.featureName, 'DICT')
    tmpFeature = loadPeopleCountFeature( grParams );     % load features from file   
else
    tmpFeature.dirs{1} = 't';
end

nPartition = 4;                                      % number of the partitions, n-fold cv

% do regression for all three directions
for iDir = 1:length(tmpFeature.dirs)
    
    % do system identification on the features to get the observations Y (oma)
    if ~strcmp(grParams.featureName, 'DICT')
        [oma, cntAvg] = sysIdOnFeature( tmpFeature.fv{iDir}, tmpFeature.cnt{iDir}, grParams );
    end
    
    for iP = 1:nPartition
        
        if strcmp(grParams.featureName, 'DICT')
            scParams.nWinSize = grParams.nWinSize;
            scParams.nWinStep = grParams.nWinStep;
            scParams.iP = iP;
            scParams.nPartition = nPartition;
            scParams.sparseCode = grParams.sparseCode;
            scParams.winTemporal = 5;
            scParams.stepTemporal = 1;
            % sparse coding
            [alphaDict, countDict] = sparseCodingCrowd(scParams);
            % pooling
            for iAlpha = 1:length(alphaDict)
                alphaPooled{iAlpha} = (cell2mat(poolingSparseCode(alphaDict{iAlpha}, grParams.poolStyle, grParams.poolDirection)))';
            end
            % system identification
            siParams.sysid = grParams.sysid;
            siParams.useWeightedSysid = grParams.useWeightedSysid;
            siParams.nSta = grParams.nSta;
            siParams.nObs = grParams.nObs;
            siParams.subtractMean = true;
            siParams.useWeightedSysid = grParams.useWeightedSysid;
            siParams.sigmaWeightedSysid = grParams.sigmaWeightedSysid;
            siParams.nWinSize = grParams.nWinSize;
            siParams.winTemporal = scParams.winTemporal;
            siParams.stepTemporal = scParams.stepTemporal;
            [oma, cntAvg] = sysIdOnVideoAndComputeIndependentValue(alphaPooled, countDict, siParams);
        end
        
        grParams.nTesting = iP:nPartition:grParams.nSys;              % the id for testing
        grParams.nSampling = 1:grParams.nSys;              
        grParams.nSampling(grParams.nTesting) = [];                   % the id for training
        [cntEst, errEst, tPos, initPnt, initVel, totEnergy] = estimateOnFeature( oma, cntAvg, grParams, iP ); % regression and prediction

        % save the plot and data
        if strcmp( grParams.featureName, 'FEAT' ) || strcmp( grParams.featureName, 'HOOF' )
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_Init%d_Part%dOf%d.fig', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.useFullInitializePiecewise, iP, nPartition);
            saveas( gca, filename );
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_Init%d_Part%dOf%d.png', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.useFullInitializePiecewise, iP, nPartition);
            saveas( gca, filename );
            % save the results
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_Init%d_Part%dOf%d.mat', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.useFullInitializePiecewise, iP, nPartition);
            save( filename, 'cntAvg', 'cntEst', 'grParams', 'errEst', 'tPos', 'initPnt', 'initVel', 'totEnergy' );
            
        elseif strcmp( grParams.featureName, 'DICT' )
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_Init%d_%s_%s_%s_Part%dOf%d.fig', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.useFullInitializePiecewise, grParams.poolStyle, grParams.poolDirection, grParams.sparseCode(1:end-4), iP, nPartition);
            saveas( gca, filename );
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_Init%d_%s_%s_%s_Part%dOf%d.png', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.useFullInitializePiecewise, grParams.poolStyle, grParams.poolDirection, grParams.sparseCode(1:end-4), iP, nPartition);
            saveas( gca, filename );
            % save the results
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_Init%d_%s_%s_%s_Part%dOf%d.mat', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.useFullInitializePiecewise, grParams.poolStyle, grParams.poolDirection, grParams.sparseCode(1:end-4), iP, nPartition);
            save( filename, 'cntAvg', 'cntEst', 'grParams', 'errEst', 'tPos', 'initPnt', 'initVel', 'totEnergy' );

        elseif strcmp( grParams.featureName, 'RAW' )
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_Init%d_ds%.2f_Part%dOf%d.fig', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.useFullInitializePiecewise, grParams.downsampling, iP, nPartition);
            saveas( gca, filename );
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_Init%d_ds%.2f_Part%dOf%d.png', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.useFullInitializePiecewise, grParams.downsampling, iP, nPartition);
            saveas( gca, filename );
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_Init%d_ds%.2f_Part%dOf%d.mat', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.useFullInitializePiecewise, grParams.downsampling, iP, nPartition);
            save( filename, 'cntAvg', 'cntEst', 'grParams', 'errEst', 'tPos', 'initPnt', 'initVel', 'totEnergy' );

        else
            error('Unknown feature name');
        end
    end
end

h = figure(1000);
filename = sprintf('%s/people_optimal_breakpoints.fig', outputPrefix);
saveas(h, filename);

%p = profile('info');
%filename = sprintf('%s/people_counting_profile', outputPrefix);
%profsave(p, filename);
