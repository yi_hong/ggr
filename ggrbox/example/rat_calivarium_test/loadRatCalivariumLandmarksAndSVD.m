% load the landmarks of rat calivarium

function [omas, ages, sigma] = loadRatCalivariumLandmarksAndSVD( grParams )

filename = sprintf( '%s/data/rat/Book-VilmannRat.txt', grParams.inputPrefix );
data = load( filename, '-ascii' );

% clean the data
flag = 1:size(data, 1);
for iI = 1:size(data, 1)
    if ~isempty(find( data(iI, :) == 9999 ))
        rvInd = find( data(:, 1) == data(iI, 1) );
        flag(rvInd) = 0;
    end
end
flag(find(flag == 0)) = [];
data = data(flag, :);

omas = cell( size(data, 1), 1 );
landmarks = cell( size(data, 1), 1);
sigma = zeros( 2, 2 );
nLandmarks = floor((size(data, 2)-2)/2);

ages = data(:, 2);
sigmaPlot = zeros(length(omas), 2);
for iI = 1:length(omas)
    tmpLM = zeros(nLandmarks, 2);
    for iJ = 1:nLandmarks
        tmpLM(iJ, :) = data(iI, iJ*2+1:iJ*2+2);
    end
    landmarks{iI} = tmpLM;
    % remove the center
    landmarks{iI} = landmarks{iI} - repmat( mean(landmarks{iI}, 1), size(landmarks{iI}, 1), 1 );
    [omas{iI}, tmpSigma, ~] = svd( landmarks{iI}, 0 );
    sigma = sigma + tmpSigma;
    sigmaPlot(iI, 1) = tmpSigma(1, 1);
    sigmaPlot(iI, 2) = tmpSigma(2, 2);
end
sigma = sigma / length(omas);
figure(103), boxplot(sigmaPlot);

% plot the landmarks
tMaxPlot = 150;
tMinPlot = 7;
clr = jet(tMaxPlot - tMinPlot + 1);
%clr = jet(max(data(:, 2)));
figure(101);
subplot(1, 3, 1), hold on
for iI = 1:length(landmarks)
    plot( landmarks{iI}(:, 1), landmarks{iI}(:, 2), '.', 'Color', clr(data(iI, 2)-tMinPlot+1, :) );
end
axis equal
hold off
title( 'Original landmarks' );
colormap(clr);
caxis([tMinPlot, tMaxPlot]);
colorbar

subplot(1, 3, 2), hold on
for iI = 1:length(omas)
    plot( omas{iI}(:, 1), omas{iI}(:, 2), '.', 'Color', clr(data(iI, 2)-tMinPlot+1, :) );
end
axis equal
hold off
title( 'U matrix' );
colormap(clr);
caxis([tMinPlot, tMaxPlot]);
colorbar

subplot(1, 3, 3), hold on
omasSigma = omas;
for iI = 1:length(omas)
    omasSigma{iI} = omasSigma{iI} * sigma;
    plot( omasSigma{iI}(:, 1), omasSigma{iI}(:, 2), '.', 'Color', clr(data(iI, 2)-tMinPlot+1, :) );
end
axis equal
hold off
title( 'U*sigma matrix' );
colormap(clr);
caxis([tMinPlot, tMaxPlot]);
colorbar
