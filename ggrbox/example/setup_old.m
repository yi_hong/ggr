% set up the system
function setup()

disp('Set up ...');
addpath(genpath('../../'));

%{
disp('Set up dtbox');
addpath('../../dtbox/systems')
addpath('../../dtbox/helper')
addpath('../../dtbox/plots')
addpath('../../dtbox/distances')
addpath('../../dtbox/data')

disp('Set up grbox');
addpath('../../grbox/regression');
addpath('../../grbox/shooting');
addpath('../../grbox/plots');
addpath('../../grbox/utils');
addpath('../../grbox/data');

disp('Set up ofbox');
addpath('../../ofbox');
addpath('../../ofbox/utils');

disp('Set up BIRSVD');
addpath('../../BIRSVD/SOURCE/LOW_RANK');
addpath('../../BIRSVD/SOURCE/UTILS');
addpath('../../BIRSVD/SOURCE/SIMUL');
%}
