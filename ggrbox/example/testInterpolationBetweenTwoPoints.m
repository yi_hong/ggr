% test 
clear all
close all
setup();

f = [1, 3];
pDim = 24;
nSta = 2;
[CC, ~, ~] = svd(randn(pDim, nSta), 0);

% generate signal
x = 0:0.05:10*pi;
sig = cell(length(f), 1);
for iI = 1:length(f)
    s = [sin(f(iI)*x); cos(f(iI)*x)];
    y = CC*s;
    y = y + 0.01*rand(size(y));
    sig{iI} = y;
end

% plot the results
tmp = CC'*sig{1};
figure, plot(tmp(1, :), 'r-', 'LineWidth', 2);
hold on
tmp = CC'*sig{2};
plot(tmp(1, :), 'b--', 'LineWidth', 2);

sigs = interpolatePoints( sig{1}, sig{2}, [0.2], nSta, 1 );
for iI = 1:length(sigs)
    tmp = CC'*sigs{iI};
    plot(tmp(1, :), 'k-.', 'LineWidth', 2);
end
hold off

