%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%  integrate forward to a given time point
%  the notation is consistent with the paper
%  Input:
%  X10: the initial point on the manifold
%  X20: the initial velocity in the tangent space
%  t: the target time point
%  hStep: the time step for integration
%  Output:
%  X1: the sequence of points on the geodesic from 0 to t
%  X2: the sequence of velocities from 0 to t
%
%  Author: Yi Hong
%  Date: 09-02-2013
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [X1, X2, t1] = integrateForwardToGivenTime( X10, X20, t, hStep )

% move backward, change the direction of velocity if t < 0
dirFlag = 1;
if t < 0 
    X20 = -X20;
    t = -t;
	dirFlag = -dirFlag;
end

X1_k = X10;
X2_k = X20;
X_k = [X1_k; X2_k];
X1 = {};
X2 = {};
nSize = size(X10, 1);
nt = floor(t/hStep);
h = hStep;

% 4-th Runge-Kutta
for it = 1:nt
	X1{it} = X_k(1:nSize, :);
    X2{it} = X_k(nSize+1:end, :);
	t1(it) = (it-1)*h*dirFlag;
	Y1_k = X_k;
    fY1 = f(Y1_k);
    Y2_k = X_k + h/2.0 * fY1;
    fY2 = f(Y2_k);
    Y3_k = X_k + h/2.0 * fY2;
    fY3 = f(Y3_k);
    Y4_k = X_k + h * fY3;
    X_k = X_k + h * ( fY1/6.0 + fY2/3.0 + fY3/3.0 + f(Y4_k)/6.0 );
    %{
    Y2_k = X_k + h/2.0 * f(Y1_k);
    Y3_k = X_k + h/2.0 * f(Y2_k);
    Y4_k = X_k + h * f(Y3_k);
    X_k = X_k + h * ( f(Y1_k)/6.0 + f(Y2_k)/3.0 + f(Y3_k)/3.0 + f(Y4_k)/6.0 );
    %}
end
X1{nt+1} = X_k(1:nSize, :);
X2{nt+1} = X_k(nSize+1:end, :);
t1(nt+1) = nt*h*dirFlag;

h = t - nt*hStep;
if h > 0
    X_k = [X1{end}; X2{end}];
    Y1_k = X_k;
    fY1 = f(Y1_k);
    Y2_k = X_k + h/2.0 * fY1;
    fY2 = f(Y2_k);
    Y3_k = X_k + h/2.0 * fY2;
    fY3 = f(Y3_k);
    Y4_k = X_k + h * fY3;
    X_k = X_k + h * ( fY1/6.0 + fY2/3.0 + fY3/3.0 + f(Y4_k)/6.0 );
    %{
    Y2_k = X_k + h/2.0 * f(Y1_k);
    Y3_k = X_k + h/2.0 * f(Y2_k);
    Y4_k = X_k + h * f(Y3_k);
    X_k = X_k + h * ( f(Y1_k)/6.0 + f(Y2_k)/3.0 + f(Y3_k)/3.0 + f(Y4_k)/6.0 );
    %}
    X1{end+1} = X_k(1:nSize, :);
    X2{end+1} = X_k(nSize+1:end, :);
	t1(end+1) = t*dirFlag;
end

% function: f(X) = (X2; -X1(X2'X2))
function Y = f(X)
nSize = floor(size(X, 1)/2);
X1 = X(1:nSize, :);
X2 = X(nSize+1:end, :);
Y1 = X2;
Y2 = -X1*(X2'*X2);
Y = [Y1;Y2];

