function opt = loadOptimizerOptions()

opt = [];

% optimizer options fmincon
options_fmincon = optimset( 'fmincon' );
options_fmincon = optimset( options_fmincon, 'Display', 'iter' );
options_fmincon = optimset( options_fmincon, 'FunValCheck', 'off' );
options_fmincon = optimset( options_fmincon, 'GradObj', 'on' );
options_fmincon = optimset( options_fmincon, 'algorithm', 'interior-point' );
options_fmincon = optimset( options_fmincon, 'Hessian', 'lbfgs' );
%options_fmincon = optimset( options_fmincon, 'MaxFunEvals', 500 );
%options_fmincon = optimset( options_fmincon, 'OutputFcn', 'augmentedfminconoutput' );

opt.fmincon = options_fmincon;