%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Implements Algorithm 2, shooting algorithm for a fixed initial point, 
%  which is in the paper, "Geodesic regression on the grassmann manifold"
% 
%  Data: params (parameters for shooting)
%          .Y0       (initial point, vector)
%          .Y1       (target point, vector)
%          .sigmaSqr (weight for similarity term)
%          .nt       (number of desired time-steps)
%          .h        (time step)
%  Result: tanY (initial velocity)
%
%  Author: Yi Hong
%  Date: 08-30-2013
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [tanY] = shootingOnGrassmannManifold( params )

% set initial velocity tanY0
tanY0 = zeros( size(params.Y0) );

% set optimization parameters
optOpts = loadOptimizerOptions();

tic

lb = -1000*ones( size(tanY0) );
ub = 1000*ones( size(tanY0) );
tanY = fmincon( @(tanY)energyAndGradientShooting(tanY, params), tanY0, ...
                [], [], [], [], lb, ub, [], optOpts.fmincon );
            
toc  



