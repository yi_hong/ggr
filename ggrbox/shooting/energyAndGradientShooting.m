%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%  compute energy and gradient for shooting
%
%  Author: Yi Hong
%  Date: 08-30-2013
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [energyTotal, gradientX20] = energyAndGradientShooting( tanY, params )

energyTotal = 0; 

% forward in time
[X1, X2, Y_RK] = integrateForward( params.Y0, tanY, params.nt, params.h );

% compute the energy of the initial velocity
energyV = trace( (X2{1})' * X2{1} );

% compute the energy of the similarity term
energyS = grarc(X1{end}, params.Y1) / params.sigmaSqr;

% compute the total energy
energyTotal = energyV + energyS;

% backward in time
%[~, lam1_end] = grgeo_direct(X1{end}, params.Y1, 1);
% use the closed form to compute the initial velocity
[~, ~, ~, lam1_end] = grgeo(X1{end}, params.Y1, 1, 'v3', 'v2');
lam1_end = lam1_end * 2.0 / params.sigmaSqr;
lam2_end = zeros( size(lam1_end) );
[lam1, lam2] = integrateBackward( lam1_end, lam2_end, Y_RK, params.nt, params.h );

% compute the gradient to update
gradientX20 = -(2*X2{1} - (eye(size(X1{1}, 1)) - X1{1}*(X1{1})') * lam2{end});
% linesearch
gradientX20 = (eye(size(params.Y0, 1)) - params.Y0*(params.Y0)') * gradientX20; 

lam2{end}
gradientX20
