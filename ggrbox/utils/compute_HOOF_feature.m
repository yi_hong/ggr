% compute HOOF features based on optical flow
function [hoof] = compute_HOOF_feature(uv, nBin)

hoof = zeros(nBin, 1);
for iI = 1:size(uv, 1)
    for iJ = 1:size(uv, 2)
        theta = atan2( uv(iI, iJ, 2), uv(iI, iJ, 1) );
        magni = norm( reshape(uv(iI, iJ, :), 2, 1), 2 );
        if theta < -pi/2
            theta = -pi - theta;
        elseif theta > pi/2
            theta = pi - theta;
        end
        index = floor( ( theta/pi + 0.5 ) * nBin ) + 1;
        index = min(index, nBin);
        index = max(index, 1);
        hoof(index) = hoof(index) + magni;
    end
end

%hoof = hoof ./ sum(hoof);