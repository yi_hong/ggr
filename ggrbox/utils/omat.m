function Q = omat(dt, k)
% OMAT Finite Observability Matrix (LDS).
%
%   [Q, O] = GRGEO(dt, k) build the finite observability matrix from 
%   a dynamical system dt (up to the power of A^k).
%
% Author: Roland Kwitt, 2013
% E-Mail: roland.kwitt@kitware.com

    n = size(dt.C,1);
    p = size(dt.C,2);
    O = zeros(k*n,p);
    O(1:n,:) = dt.C;
    for i=1:k-1
        O(i*n+1:(i+1)*n,:) = dt.C*dt.A^i;
    end
    [Q,~] = qr(O,0);
end