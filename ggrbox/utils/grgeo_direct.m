function [R,T] = grgeo_direct(varargin)
% GRGEO_DIRECT Move along geodesic connecting two points A and B
% (subspaces) on a Grassmann manifold.
%
% [R,T] = GRGEO_DIRECT(A,B,t) computes the element R along the geodesic g
% connecting the subspaces S_A and S_B represented by A and B (orthonormal 
% n x p matrices); g: [0,1] -> Gr(p,n), t |-> R, s.t., g(1) = span(B) and 
% g(0) = span(A). T is the (n x p) in the tangent space at S_A pointing in 
% the direction of S_A, s.t., exp_{S_A}(T) = S_B.
%
% Example:
%   
%   A = orth(rand(10,3));
%   B = orth(rand(10,3));
%   [R,T] = grgeo_direct(A, B, 0);
%   fprintf('d(S_A, S_R) = %.4f\n', grarc(A, R))
%   fprintf('||A^t*T||_F = %.4f\n', norm(A'*T, 'fro'));

    R = []; % Element along geodesic
    T = []; % Element in tangent space, not set yet

    t = 1;
    tol = 1e-6;
    
    if nargin < 2
        return
    end

    % Subspace representers
    A = varargin{1};
    B = varargin{2};
    
    [n,p] = size(A);
    assert(n == size(B,1));
    assert(p == size(B,2));
    
    if nargin == 3
        t = varargin{3};
    end
    
    if nargin == 4
        t = varargin{3};
        rng(varargin{4}, 'twister');
    end
    
    Qt = orthoc(A);
    Q = Qt';
    %assert(norm(diag(diag(Q'*A))-eye(p,p),'fro') < tol)

    tmp = Q'*B;
    %assert(norm(tmp(1:p,:)-A'*B,'fro') < tol)

    tmp = Q'*B;
    %assert(norm(tmp(p+1:end,:) - Q(:,p+1:end)'*B, 'fro') < tol)
    
    tmp = Q'*B;
    t0 = tmp(1:p,:);
    t1 = tmp(p+1:end,:);
    [V,U1,W,C,S] = gsvd(t0, t1);

    %assert(norm(C'*C + S'*S - eye(p,p),'fro') < tol)
    %assert(norm(A'*B - V*C*W', 'fro') < tol)
    %assert(norm(Q(:,p+1:end)'*B - U1*S*W', 'fro') < tol)

    A_o = Q(:,p+1:end);  % ortho-complement of A
    U = A_o*U1;
    S1 = S;
 
    %assert(norm(A*A'*B + A_o*A_o'*B - B, 'fro') < tol);
    %assert(norm(A*V*C*W' - A*A'*B, 'fro') < tol);
    %assert(norm(U*S1*W' - A_o*A_o'*B,'fro') < tol);
 
    x = acos(diag(C));
    R = [A*V U(:,1:p)]*[diag(cos(x*t)); diag(sin(x*t))]*V';
    T = U*S1*V';
end










