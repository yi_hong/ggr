% check constraints 
function checkConstraints(Yend, Y0, Y0dot)

disp( 'Check the integrated-forward points stay on the Grassmanian manifold ...' );
for iI = 1:length(Yend)
    strTmp = sprintf('%d -- f-norm distance to id: %f', ...
             iI, norm( (Yend{iI})'*Yend{iI} - eye(size(Yend{iI}, 2)), 'fro' ) );
    disp(strTmp);
end

disp( 'Check the initial velocity and initial point ...' );
strTmp = sprintf( 'f-norm distance to zero: %f', norm( Y0dot' * Y0, 'fro' ) );
disp(strTmp);

