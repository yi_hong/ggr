% 
% Compute testing error 
% Author: Yi Hong
% Email: yihong@cs.unc.edu
%

function [err, omaEst] = computeGeodesicTestError( initPntAll, initVelAll, tPosAll, timeWarpParams, omaTest, tTest )

if ~isempty(omaTest)
    assert( length(omaTest) == length(tTest) );
end
err = zeros(length(tTest), 1);

tUnique = unique(tTest);
omaEst = cell(length(tTest), 1);

if length(initPntAll) == length(initVelAll) && length(initVelAll) == 1   % only one piece
    fprintf('Pointwise or Full GGR (time-warped or unwarped)\n');
    initPnt = initPntAll{1};
    initVel = initVelAll{1};
    tPos = tPosAll(1);
    for iI = 1:length(tUnique)
        if ~isempty(timeWarpParams)
            tEnd = generalisedLogisticFunction( tUnique(iI), timeWarpParams );
        else
            tEnd = tUnique(iI);
        end
        clear omaTmp
        if abs( tEnd - tPos ) < 1e-7
            omaTmp{1} = initPnt;
        else
            [~, omaTmp] = integrateForwardWithODE45( initPnt, initVel, [tPos (tPos+tEnd)/2 tEnd] );
        end
        indTmp = find( tTest == tUnique(iI) );
        for iJ = 1:length(indTmp)
            if isempty(omaTest)
                err(indTmp(iJ)) = NaN;
            else
                err(indTmp(iJ)) = grarc( omaTmp{end}, omaTest{indTmp(iJ)} );
            end
            omaEst{indTmp(iJ)} = omaTmp{end};
        end
    end
    
elseif length(initPntAll) == 1 && length(initVelAll) > 1    % multiple pieces, while continuous
    fprintf('Continuous piecewise GGR\n');
    initPnt = initPntAll{1};
    initVel = initVelAll{1};
    tPos = tPosAll(1);
    for iI = 1:length(tUnique)
        tEnd = tUnique(iI);
        clear omaTmp
        if abs( tEnd - tPos ) < 1e-7
            omaTmp{1} = initPnt;
        elseif tEnd < tPos
            [~, omaTmp] = integrateForwardWithODE45( initPnt, initVel, [tPos (tPos+tEnd)/2 tEnd] );
        else
            idPiece = length(find(tPosAll < tEnd));
            for iJ = 1:idPiece-1
                tPosNext = tPosAll(iJ+1);
                [~, tmpPnt] = integrateForwardWithODE45( initPnt, initVel, [tPos (tPos+tPosNext)/2 tPosNext] );
                initPnt = tmpPnt{end};
                initVel = initVelAll{iJ+1};
                tPos = tPosNext;
            end
            if abs(tEnd - tPos) < 1e-7
                omaTmp{1} = initPnt;
            else
                [~, omaTmp] = integrateForwardWithODE45( initPnt, initVel, [tPos (tPos+tEnd)/2 tEnd] );
            end
        end
        indTmp = find( tTest == tUnique(iI) );
        for iJ = 1:length(indTmp)
            if isempty(omaTest)
                err(indTmp(iJ)) = NaN;
            else
                err(indTmp(iJ)) = grarc( omaTmp{end}, omaTest{indTmp(iJ)} );
            end
            omaEst{indTmp(iJ)} = omaTmp{end};
        end
    end
    
elseif length(initPntAll) > 1 && length(initPntAll) == length(initVelAll)  % multiple pieces, not continous
    fprintf('Piecewise GGR\n');
    initPnt = initPntAll{1};
    initVel = initVelAll{1};
    tPos = tPosAll(1);
    for iI = 1:length(tUnique)
        tEnd = tUnique(iI);
        clear omaTmp
        if abs( tEnd - tPos ) < 1e-7
            omaTmp{1} = initPnt;
        elseif tEnd < tPos
            [~, omaTmp] = integrateForwardWithODE45( initPnt, initVel, [tPos (tPos+tEnd)/2 tEnd] );
        else
            idPiece = length(find(tPosAll < tEnd));
            initPnt = initPntAll{idPiece};
            initVel = initVelAll{idPiece};
            tPos = tPosAll(idPiece);
            if abs(tEnd - tPos) < 1e-7
                omaTmp{1} = initPnt;
            else
                [~, omaTmp] = integrateForwardWithODE45( initPnt, initVel, [tPos (tPos+tEnd)/2 tEnd] );
            end
        end
        indTmp = find( tTest == tUnique(iI) );
        for iJ = 1:length(indTmp)
            if isempty(omaTest)
                err(indTmp(iJ)) = NaN;
            else
                err(indTmp(iJ)) = grarc( omaTmp{end}, omaTest{indTmp(iJ)} );
            end
            omaEst{indTmp(iJ)} = omaTmp{end};
        end
    end
else
    error('Unknown type of regression');
end
