% 
% compute the R2 statistic
% Author: Yi Hong
% Email: yihong@cs.unc.edu
%

function R2 = computeR2Statistic( initPnt, initVel, tPos, timeWarpParams, yOmas, tAges, grParams )

% only for training data
ages = tAges( grParams.nSampling );
omas = yOmas( grParams.nSampling );

% plot the points on the geodesic
figure(400);
for iPlot = 1:length(initPnt)
    
    % need to check the part for piecewise regression because I'm now
    % focusing on the full GGR and time warped regression
    if strcmp( grParams.regressMethod{iPlot}, 'PiecewiseRegression' ) || ...
       strcmp( grParams.regressMethod{iPlot}, 'ContinuousPiecewiseRegression' ) || ...
       strcmp( grParams.regressMethod{iPlot}, 'ContinuousPiecewiseRegressionWithOptimalBreakpoints' )
        tSpanOrig = [unique(ages') grParams.maxValue-1];
        interval = [ tPos{iPlot} grParams.maxValue ];
        for itPos = 1:length(interval)-1
            indexSpan = find( tSpanOrig >= interval(itPos) & tSpanOrig < interval(itPos+1) );
            tSpan = tSpanOrig(indexSpan);
            tSpan = [tSpan interval(itPos+1)];
            flag = false;                 
            if tSpan(1) ~= interval(itPos)
                tSpan = [interval(itPos) tSpan];
                flag = true;
            end
            if itPos == 1
                [tTmp, tmp] = integrateForwardWithODE45( initPnt{iPlot}{itPos}, initVel{iPlot}{itPos}, tSpan );
            else
                if strcmp( grParams.regressMethod{iPlot}, 'PiecewiseRegression' )
                    initPntNext = initPnt{iPlot}{itPos};
                else
                    initPntNext = tmp{end};
                end
                [tTmp, tmp] = integrateForwardWithODE45( initPntNext, initVel{iPlot}{itPos}, tSpan );
            end
            if itPos == 1
                if flag
                    pointsSyn = tmp(2:end-1);
                    t{iPlot} = tTmp(2:end-1);
                else
                    pointsSyn = tmp(1:end-1);
                    t{iPlot} = tTmp(1:end-1);
                end
            else
                if flag
                    pointsSyn = [pointsSyn; tmp(2:end-1)];
                    t{iPlot} = [t{iPlot}; tTmp(2:end-1)];
                else
                    pointsSyn = [pointsSyn; tmp(1:end-1)];
                    t{iPlot} = [t{iPlot}; tTmp(1:end-1)];
                end
            end
        end
        
        omasGeodesic = cell(length(ages), 1);
        for iITmp = 1:length(omasGeodesic)
            indTmp = find( abs(t{iPlot} - ages(iITmp) ) < 1e-7 );
            omasGeodesic{iITmp} = pointsSyn{indTmp(1)};
        end
        
    elseif strcmp( grParams.regressMethod{iPlot}, 'timeWarpRegression' )
        % time warped regression
        
        figure(701), hold on;
        % plot the optimized logistic function
        plot( ages, generalisedLogisticFunction(ages, timeWarpParams{iPlot}), 'ro-.', 'LineWidth', 2 );
        % plot the M point
        plot( timeWarpParams{iPlot}.M*[1 1], [0 generalisedLogisticFunction( timeWarpParams{iPlot}.M, ...
            timeWarpParams{iPlot} ) ], 'r--', 'LineWidth', 2 );
        hold off;

        % time points for shooting forward
        tSpan = sort(generalisedLogisticFunction( unique(ages'), timeWarpParams{iPlot} ));
        % make sure the tSpan starting from the correct starting point
        assert( abs( tSpan(1) - tPos{iPlot}(1) ) < 1e-7 );
        % shooting forward
        [t{iPlot}, pointsSyn] = integrateForwardWithODE45( initPnt{iPlot}{1}, initVel{iPlot}{1}, unique(tSpan) );
        twAges = generalisedLogisticFunction(ages', timeWarpParams{iPlot});
        omasGeodesic = cell(length(ages), 1);
        for iITmp = 1:length(omasGeodesic)
            indTmp = find( abs(t{iPlot} - twAges(iITmp) ) < 1e-7 );
            omasGeodesic{iITmp} = pointsSyn{indTmp(1)};
        end
        
        % go back to real time
        %t{iPlot} = inverseLogisticFunction( t{iPlot}, timeWarpParams{iPlot} );
    else
        tSpan = sort(unique(ages));
        assert( tSpan(1) == tPos{iPlot}(1) );
        [t{iPlot}, pointsSyn] = integrateForwardWithODE45( initPnt{iPlot}{1}, initVel{iPlot}{1}, tSpan );
        omasGeodesic = cell(length(ages), 1);
        for iITmp = 1:length(omasGeodesic)
            indTmp = find(abs(t{iPlot} - ages(iITmp)) < 1e-7);
            omasGeodesic{iITmp} = pointsSyn{indTmp(1)};
        end
    end

    % compute R^2 statistics
    R2(iPlot) = 1 - computeVar(omasGeodesic, omas, 3) / computeVar(omas, omas, 1);
end