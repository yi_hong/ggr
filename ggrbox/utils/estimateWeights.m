% compute the weight for data points
% Note: the sum of the weights is equal to the number of the data, not 1
function [weight] = estimateWeights( data, useWeight )

weight = ones(size(data));
if useWeight
    [weight, xi] = ksdensity(data, data, 'bandwidth', 10);
    weight = 1.0 ./ weight;
    weight = weight ./ sum(weight) * length(weight);
end

%{
if useWeight
    [~, density, xmesh, ~] = kde(data);
    for iI = 1:length(data)
        [~, index] = min( abs(xmesh - data(iI)) );
        weight(iI) = 1.0/density(index);
    end
    weight = weight ./ sum(weight) * length(weight);
end
%}

%{ 
% weights based on histogram, for future use
nHist = histc( cntTrain, grParams.histEdges );
if length(nHist) > 2 
    nHist(end-1) = nHist(end-1) + nHist(end);
end
grParams.weight = zeros(length(cntTrain), 1);
for iI = 1:length(cntTrain)
    for iJ = 1:length(grParams.histEdges)-1
        if cntTrain(iI) >= grParams.histEdges(iJ) && cntTrain(iI) <= grParams.histEdges(iJ+1)
            grParams.weight(iI) = 1.0 / nHist(iJ);
            break;
        end
    end
end
grParams.weight = grParams.weight ./ sum(grParams.weight) * length(grParams.weight);
%}