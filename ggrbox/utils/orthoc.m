function Qt = orthoc(A)
    [u,v] = size(A);
    A_1 = A(1:v,1:v);
    A_2 = A(v+1:end,:);
    Qt = eye(u)-[A_1-eye(v);A_2]*pinv(eye(v)-A_1)*[A_1'-eye(v),A_2'];
end