% test spline regression

flag = 1;  % 0: linear; >0: spline

n = 2;
p = 1;
X10 = rand(n, p);
[X10, ~] = qr(X10, 0);

X20 = (eye(n, n) - X10*X10')*rand(n, p);

X10'*X10
X10'*X20

if flag == 0
    X30 = zeros(n, p);
    X40 = zeros(n, p);
else
    X30 = (eye(n, n) - X10*X10')*rand(n, p);
    X30 = X30 / norm(X30, 2);
    %X40 = (eye(n, n) - X10*X10')*rand(n, p);
    %X30 = rand(n, p);
    X40 = rand(n, p);
    %X40 = zeros(n, p);
end

nt = 10000;
h = 0.01;

[X1, X2, X3, X4, Y_RK] = integrateForwardSpline( X10, X20, X30, X40, nt, h );

%[X1, X2, X3, X4, Tend] = integrateForwardWithODE45Spline( X10, X20, X30, X40, 0:h:nt*h );

errX1 = zeros(1, length(X1));
errX2 = zeros(1, length(X2));
if flag > 0
    errX3 = zeros(1, length(X3));
end
dist = zeros(1, length(X1));
for iI = 1:length(X1)
    errX1(iI) = norm( X1{iI}'*X1{iI} - eye(p, p), 'fro' );
    errX2(iI) = norm( X1{iI}'*X2{iI}, 'fro' );
    if flag > 0
        errX3(iI) = norm( X1{iI}'*X3{iI}, 'fro' );
    end
    dist(iI) = sqrt(grarc(X1{iI}, X10));
end

figure, plot(errX1, 'r--');
hold on
plot(errX2, 'g--');
if flag == 0
    legend('X1''X1-I', 'X1''X2');
else
    plot(errX3, 'b-.');
    legend('X1''X1-I', 'X1''X2', 'X1''X3');
end
set(gca, 'FontSize', 14);

figure, plot(dist);