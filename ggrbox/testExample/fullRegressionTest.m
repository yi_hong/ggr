%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  test shooting on Grassmannian manifold
%
%  Author: Yi Hong
%  Date: 08-30-2013
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clear all

setup();

dt.A = [0 1; -1 0];
dt.C = eye(2);
Y1 = omat(dt, 2);

dt.A = [0 1; -4 0];
dt.C = eye(2);
Y2 = omat(dt, 2);

dt.A = [0 1; -9 0];
dt.C = eye(2);
Y3 = omat(dt, 2);

dt.A = [0 1; -16 0];
dt.C = eye(2);
Y4 = omat(dt, 2);

dt.A = [0 1; -25 0];
dt.C = eye(2);
Y5 = omat(dt, 2);

% set params 

params.Ys{1} = Y1;
params.ts(1) = 1;
params.Ys{2} = Y2;
params.ts(2) = 2;
params.Ys{3} = Y3;
params.ts(3) = 3;
%params.Ys{4} = Y4;
%params.ts(4) = 4;
%params.Ys{5} = Y5;
%params.ts(5) = 5;
params.t0 = min(params.ts);
params.nt = 50;
params.h = 1./params.nt;
params.sigmaSqr = .1;
params.deltaT = 0.05;
params.maxReductionSteps = 20;
params.rho = 0.5;
params.nIterMax = 500;
params.c1 = 1e-4;
params.minFunc = 'linesearch';   % linesearch, minFunc, fmincon
params.nsize = size(params.Ys{1});

% do shooting
[pntY, tanY] = fullRegressionOnGrassmannManifold( params );

% move forward based on initial conditions
[X1, X2] = integrateForwardMP( pntY, tanY, params.nt, params.h );

% plot the results
figure, hold on;
xList = 0:params.h:1;
distSqr = zeros( length(params.Ys), 1 );
maxTs = max( params.ts - params.t0 );
for iI = 1:length(params.Ys)
    index = round ( (params.ts(iI)-params.t0) / maxTs / params.h ) + 1;
    distSqr(iI) = grarc( params.Ys{iI}, X1{index} );
end
plot( params.ts, distSqr, 'bo', 'LineWidth', 2 );
hold off

distSqr

%{
plot( reshape( params.Ys{1}, [], 1 ), 'b', 'LineWidth', 2 );
plot( reshape( params.Ys{2}, [], 1 ), 'r', 'LineWidth', 2 );
plot( reshape( params.Ys{3}, [], 1 ), 'g', 'LineWidth', 2 );
cmap = cool(length(X1));
for iI = 1:length(X1)
    plot( reshape( X1{iI}, [], 1 ), '--', 'Color', cmap(iI, :) );
end
hold off
colormap(cmap);
colorbar
%}

