% 
% test spline regression using synthetic data
% 
% Author: Yi Hong, yihong@cs.unc.edu
%
% Date: 08/25/2014
%

clear all
close all

%% generate data using system identification
step=0.05; x = 0:step:10*pi;

% Frequencies within (0, 10]
nTotalNum = 25;
%f = sort(rand(1, nTotalNum)*10);
minBoundary = 0.2;
maxBoundary = 9.8;
%minBoundary = 0.5;       % the minimum frequency
%maxBoundary = 10.5;      % the maximum frequency
t = linspace(minBoundary, maxBoundary, nTotalNum);
f = 5*(sin(t)+1);

pDim = 24;
nSta = 2;
kObs = 2;

% Linear projector into 24-D
[CC, ~, ~] = svd(randn(pDim, nSta), 0);
 
sig = cell(length(f),1); % 24-d signals
sys = cell(length(f),1); % LDS
oma = cell(length(f),1); % Finite observ. matrices
org = cell(length(f),1); % Original signal

% system identification
useWeightedSystemIdentification = false;
for i=1:length(f)
    s = [sin(f(i)*x); cos(f(i)*x)];
    org{i} = s;
    y =  CC*s;
    y = y + 0.01*rand(size(y));
    sig{i} = y;
    params.class = 2;
    
    if ( ~useWeightedSystemIdentification )
        % use uniform weights
        weights = ones( 1, size( y, 2 ) );
        weights = weights/sum(weights);      % normalize
    else
        % create Gaussian weights for test
        gVal = linspace( -3, 3, size( y,2 ) );
        weights = exp( -gVal.^2 );
        weights = weights/sum(weights);      % normalize
    end
    sys{i} = weightedSuboptimalSystemID(sig{i},nSta,weights,params);
    oma{i} = omat(sys{i}, kObs);
end
% plot the generate data
plotGeodesic( oma, t, pDim, '-*' );

%% given the initial conditions to generate data
X10 = oma{1};
[~, ~, ~, X20] = grgeo(oma{1}, oma{6}, 1, 'v3', 'v2');
[~, ~, ~, X30] = grgeo(oma{1}, oma{3}, 1, 'v3', 'v2');
X4s{1} = rand(size(X10));
X4s{2} = rand(size(X10));
params.h = 0.01;
params.Gns(1) = 50;
params.Gns(2) = 50;
[X1s] = integrateForwardEachIntervalSpline( X10, X20, X30, X4s, params );
ts(1, :) = (0:params.Gns(1))*params.h;
ts(2, :) = (params.Gns(1):params.Gns(1)+params.Gns(2))*params.h;
ts = ts(:);
Tpnt = 0:0.05:1;
Ypnt = cell(length(Tpnt), 1);
for iI = 1:length(Tpnt)
    [~, tmpPos] = min(abs(ts - Tpnt(iI)));
    Ypnt{iI} = X1s{2-mod(tmpPos, 2)}{ceil(tmpPos/2)};
end
figure, plotGeodesic( Ypnt, Tpnt, pDim, '--' );

%% perform spline regression
grParams.sigmaSqr = 1;            % sigma square for balancing the mismatching term
grParams.alpha = 0;               % alpha for balancing the prior knowledge of the slope of the geodesic
grParams.nt = 100;                % the number of the discretized grids 
grParams.h = 1./grParams.nt;
grParams.deltaT = 0.1;           % the step size for updating the initial conditions
grParams.maxReductionSteps = 10;  % the number of times for tracing back
grParams.rho = 0.5;               % the step size for tracing back
grParams.nIterMax = 2000;          % the number of iterations for updating the initial conditions
grParams.stopThreshold = 1e-10;    % the threshold for stopping the iterations
grParams.useODE45 = true;         % chose ODE45 for solving the PDEs, more stable
grParams.minFunc = 'linesearch';  % use linesearch for updating the initial conditions, other methods could be implemented later

grParams.ts = Tpnt;
grParams.Ys = Ypnt;
grParams.wi = ones(length(t), 1);
grParams.cps = [(grParams.ts(1)+grParams.ts(end))/2];     % control points
grParams.t_truth = Tpnt;
grParams.Y_truth = Ypnt;
grParams.pDim = pDim;
grParams.X10 = X10;
grParams.X20 = X20;
%grParams.X30 = X30;
%grParams.X4s = X4s;

grParams.fidSplineEnergy = 500;
grParams.fidSplineFitting = 100;

cubicSplineRegression(grParams);