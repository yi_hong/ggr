function [params] = theta2param(theta)

params.A = theta(1);
params.K = theta(2);
params.beta = theta(3);
params.k = theta(4);
params.M = theta(5);
params.m = theta(6);