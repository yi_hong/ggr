% set the parameters of time warped regression to be zero

function [params] = resetTimeWarpParams()

params.A = 0;
params.K = 0;
params.beta = 0;
params.k = 0;
params.M = 0;
params.m = 0;