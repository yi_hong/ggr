% plot the logistic function
clear all
close all

ts = 2:2:20;

params = initializeTimeWarpParams ( ts );

figure, 
plot(ts, generalisedLogisticFunction(ts, params), 'r-');
hold on 
plot(ts, inverseLogisticFunction(ts, params), 'b--' );
grid on
box on
