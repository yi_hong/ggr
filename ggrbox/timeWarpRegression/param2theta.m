function [theta] = param2theta(params)

theta(1) = params.A;
theta(2) = params.K;
theta(3) = params.beta;
theta(4) = params.k;
theta(5) = params.M;
theta(6) = params.m;