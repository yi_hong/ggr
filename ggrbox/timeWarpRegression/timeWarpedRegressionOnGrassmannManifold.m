%
% time warped regression
% Author: Yi Hong
% Email: yihong@cs.unc.edu
% 

function [Y0, Y0dot, currentEnergy, tsWarped, timeWarpParams] = timeWarpedRegressionOnGrassmannManifold( params )

% initialize the time-warp parameters
[timeWarpParamsInit] = initializeTimeWarpParams( params );
figure(701), plot( params.ts, generalisedLogisticFunction(params.ts, timeWarpParamsInit), 'b*-', 'LineWidth', 2);
hold on
plot( timeWarpParamsInit.M*[1 1], [0 generalisedLogisticFunction( timeWarpParamsInit.M, timeWarpParamsInit ) ], 'b--', 'LineWidth', 2 );
hold off
title('Generalised Logistical Funciton');

if strcmp( params.twMinFunc, 'linesearch' )
    [Y0, Y0dot, currentEnergy, tsWarped, timeWarpParams] = lineSearchTimeWarpRegression( params, timeWarpParamsInit );

elseif strcmp( params.twMinFunc, 'fmincon' )
    theta0 = param2theta( timeWarpParamsInit );
    
    lb = [-Inf -Inf -Inf 0 0 -Inf];
    ub = [Inf Inf Inf Inf Inf Inf];
    optOpts = loadTimeWarpRegressionOptimizerOptions();
    [theta] = fmincon( @(theta)energyAndGradientTimeWarpRegression( theta, params ), [theta0], ...
        [], [], [], [], lb, ub, [], optOpts.fmincon );
    
    timeWarpParams = theta2param( theta );

    % 1) compute new timepoints 
    tsWarped = generalisedLogisticFunction(params.ts, timeWarpParams);

    % 2) compute standard geodesic regression
    paramsFullGGR = params;
    paramsFullGGR.ts = tsWarped;
    [Y0, Y0dot, currentEnergy] = fullRegressionOnGrassmannManifold( paramsFullGGR );

else
    error( 'Not supported optimization method' );
end

