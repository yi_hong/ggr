% line search for time warp regression

function [Y0, Y0dot, currentEnergy, tsWarped, timeWarpParams] = lineSearchTimeWarpRegression( params, timeWarpParamsInit )

timeWarpParams = timeWarpParamsInit;

% 1) compute new timepoints 
tsWarped = generalisedLogisticFunction(params.ts, timeWarpParams);
%figure, plot(params.ts, tsWarped, 'r*-');
%title('Initialization');
    
% 2) compute standard geodesic regression
paramsFullGGR = params;
paramsFullGGR.ts = tsWarped;
paramsFullGGR.timeWarpRegression = true;
[Y0, Y0dot, currentEnergy] = fullRegressionOnGrassmannManifold( paramsFullGGR );
eng_pre = currentEnergy;
fprintf( 'Time warp regression (TWFGGR) Intial energy = %f\n', currentEnergy );

stopIteration = false;
for iI = 1:params.nIterMaxTimeWarp
    
    % 3) compute the gradient for time warp parameters
    [tsWarpedSort, indSort] = sort(tsWarped);
    YsSort = params.Ys(indSort);
    tsSort = params.ts(indSort);
    inpFlag = false;
    tSpan = unique(tsWarpedSort);
    assert( length(tSpan) >= 2 );
    if length(tSpan) == 2 
        tSpan = [tSpan(1) (tSpan(1)+tSpan(2))/2.0 tSpan(2)];
        inpFlag = true;
    end
    [tsGeo, YsGeo, YDotsGeo] = integrateForwardWithODE45( Y0, Y0dot/(max(tsWarped) - min(tsWarped)), tSpan );
    if inpFlag
        tsGeo(2) = [];
        YsGeo(2) = [];
        YDotsGeo(2) = [];
    end
    
    % plot the points and geodesic
    if isfield( params, 'pDim' )
        figure(600), plotGeodesic( params.Ys, params.ts, params.pDim, '.' );
        hold on
        plotGeodesic( YsGeo, inverseLogisticFunction(tsGeo, timeWarpParams), params.pDim, '-' );
        hold off
    end
    
    gradTimeWarpParams = resetTimeWarpParams();
    for iTs = 1:length(YsSort)
        indGeo = find( tsGeo == tsWarpedSort(iTs) );
        [~, ~, ~, vTmp] = grgeo( YsGeo{indGeo}, YsSort{iTs}, 1, 'v3', 'v2' );
        trTmp = 2 * trace( vTmp' * YDotsGeo{indGeo} ) / (-params.sigmaSqr);
        [devParams] = computeDerivativeLogisticFunction( timeWarpParams, tsSort(iTs) );
        gradTimeWarpParams = addTwoParams( gradTimeWarpParams, devParams, trTmp );
    end
    
    % 4) Update parameters
    dt = params.deltaTWarp;
    energyReduced = false;
    curParams = timeWarpParams;
    curY0 = Y0;
    curY0dot = Y0dot;
    curTsWarped = tsWarped;
    for iJ = 1:params.maxReductionSteps
        timeWarpParams = addTwoParams( timeWarpParams, gradTimeWarpParams, -dt );
        if timeWarpParams.M < 0
            timeWarpParams.M = timeWarpParams.M + dt * gradTimeWarpParams.M;
        end
        if timeWarpParams.k < 0 
            timeWarpParams.k = timeWarpParams.k + dt * gradTimeWarpParams.k;
        end
        
        % 1) compute new timepoints 
        tsWarped = generalisedLogisticFunction(params.ts, timeWarpParams);

        % 2) compute standard geodesic regression
        paramsFullGGR = params;
        paramsFullGGR.ts = tsWarped;
        paramsFullGGR.timeWarpRegression = true;
        [Y0, Y0dot, energy] = fullRegressionOnGrassmannManifold( paramsFullGGR );
        
        if energy >= currentEnergy
            fprintf('#');
        else
            currentEnergy = energy;
            energyReduced = true;
            fprintf( 'TWFGGR Iter %04d: Energy = %f\n', iI, currentEnergy );
            
            if eng_pre - currentEnergy <= params.stopThreshold
                stopIteration = true;
            end
            eng_pre = currentEnergy;
            
            break;
        end
        dt = dt * params.rho;
    end
    
    if ~energyReduced
        fprintf( 'TWFGGR Iteration %04d: Could not reduce energy further. Quitting.\n', iI );
        Y0 = curY0;
        Y0dot = curY0dot;
        timeWarpParams = curParams;
        tsWarped = curTsWarped;
        break;
    end
    
    if stopIteration
        fprintf( 'TWFGGR Quitting because the decreased energy is smaller than the threshold.\n' );
        break;
    end
end