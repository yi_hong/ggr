% derivative of inverse logistic function with respect to its parameters
% at time point ti
function [devParams] = computeDerivativeInverseLogisticFunction( params, ti )

% do not change the asymptotes 
devParams.A = 0;
devParams.K = 0;

% compute for other parameters
devParams.M = 1;

tmp0 = (params.K - params.A)/(ti - params.A);
tmp1 = tmp0^params.m - 1;
tmp2 = tmp1 / params.beta;
devParams.k = 1.0 / ((params.k)^2) * log(tmp2);
devParams.beta = -1.0 / params.k / tmp2 * tmp1 / (-params.beta^2);
devParams.m = -1.0 / params.k / tmp2 / params.beta * (tmp1+1) * log(tmp0);
