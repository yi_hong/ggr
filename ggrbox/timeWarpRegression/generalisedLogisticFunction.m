% generalized logistic curve

function [yt] = generalisedLogisticFunction( t, params )

tmp = params.beta .* exp( -params.k .* ( t - params.M) ); 
yt = params.A + (params.K - params.A) ./ ( ( 1 + tmp ).^( 1.0 / params.m) );