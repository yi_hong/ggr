clear all;
close all;

addpath 'gpml-matlab-v3.4-2013-11-11'
startup();

fprintf('To run example press [ENTER]');
pause;

load 'toyData'

sn = 2;

N = length(toyData.oMat);
X = [];
for i=1:N
    oMat = toyData.oMat{i};
    nCol = size(oMat,2);
    nRow = size(oMat,1);
    X = [X [nRow;nCol;oMat(:)]];
end

X = X';
y = toyData.y(:);

% zero-mean outputs
yMean = mean(y);   
y = y - yMean;

% run LOO eval.
yHat = zeros(N,1);
for i=1:N
    trInd = 1:N;
    trInd(i) = [];
    teInd = i;
    
    % learn the parameters
    gpm = gpLearn(X(trInd,:),y(trInd),sn,{'meanZero'},{'covBC'});

    % now, guess ...
    yHat(i) = gpGuess(gpm, X(teInd,:));
end

figure;hold;
r = linspace(min(y),max(y),100);
plot(r,r,'-', 'Linewidth',1);
plot(yHat,y,'r+');
legend({'Optimal', 'Predicted'});
meanAbsErr = mean(abs(yHat-y));
title(sprintf('MAE: %.4f', meanAbsErr));
box on;









