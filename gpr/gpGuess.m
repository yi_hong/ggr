function yHat = gpGuess(gpm, Xstar)
% GPGUESS GP prediction
%
% Author: Roland Kwitt, Univ. of Salzburg, 2013

    [~,~,yHat] = gp(gpm.dnlZ, @infExact, gpm.meanFunc, gpm.covFunc, ...
        @likGauss, gpm.X, gpm.y, Xstar);
end