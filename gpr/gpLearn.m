function gpm = gpLearn(X,y,sn,meanFunc,covFunc)
% GPLEARN Learn GP parameters for Grassmannian GP regression
% 
% Author: Roland Kwitt, Univ. of Salzburg, 2013

    if ~exist('gp')
      error('Could not find GPML in path!');
    end

    % set the noise variance
    hyp.lik = log(sn);

    % learn the GPM (using exact inference)
    [nlZ, dnlZ] = gp(hyp, @infExact, meanFunc, covFunc, @likGauss, X, y);
    fprintf('Negative log marginal LL: %.4f\n', nlZ);
    
    gpm.y = y;
    gpm.X = X;
    gpm.nlZ = nlZ;
    gpm.dnlZ = dnlZ;
    gpm.meanFunc = meanFunc;
    gpm.covFunc = covFunc;
end