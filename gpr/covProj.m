function K = covProj(hyp, x, z, i)
% COVPROJ Projection kernel for points on the Grassmannian
%
% x, and z are typically NxD and MxD matrices, where N is the number of
% training samples and M is the number of test samples (if provided, see
% covFunctions.m).
%
% Since representers on the Grassmannian are matrices, we have to reshape
% the column vectors in x and z into matrices for kernel computation. For
% that reason, this function expects that the FIRST TWO elements of each
% column vector contain the ROWs and COLUMNs of the matrices. These values
% are then used for reshaping.
%
% The projection kernel is defined in
%
%   Hamm, J. and Lee, D.D.,
%   "Grassmann Discriminant Analysis: a Unifying View on 
%   Subspace-Based Learning",
%   ICML 2008
% 
% as
%
%   k(X,Y) = ||X^T * Z||_{fro}^2
%
% There are NO hyperparameters, i.e.,
%
% hyp = [ ]
%
% Author: Roland Kwitt, Univ. of Salzburg, 2013

    if nargin<2, K = '0'; 
        return; 
    end
    
    if nargin<3, 
        z = []; 
    end
    xeqz = numel(z)==0; dg = strcmp(z,'diag') && numel(z)>0;        

    D = x(:,3:end);
    n = size(D,1);
    nRow = x(1,1);
    nCol = x(1,2); 

    if dg
        K = zeros(n,1);
        for j=1:size(D,1)
            O = reshape(D(j,:),[nRow nCol]);
            K(j) = norm(O'*O, 'fro')^2; 
        end
    else
        if xeqz
            K = zeros(n,n);
            for i=1:n
                for j=1:n
                    Oi = reshape(D(i,:),[nRow nCol]);   
                    Oj = reshape(D(j,:),[nRow nCol]);
                    K(i,j) = norm(Oi'*Oj, 'fro')^2;
                end
            end
        else
            E = z(:,3:end);
            m = size(E,1);
            K = zeros(n,m);
            for i=1:n
                for j=1:m
                    Oi = reshape(D(i,:),[nRow nCol]);   
                    Oj = reshape(E(j,:),[nRow nCol]);
                    K(i,j) = norm(Oi'*Oj, 'fro')^2;
                end
            end
        end
    end

    if nargin>3
        error('Unknown hyperparameter')
    end
end