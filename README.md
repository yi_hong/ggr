## Grassmannian Geodesic Regression (GGR) ##

This is a Matlab library for geodesic regression on the Grassmann manifold. 
The algorithms implemented in this library were published in the following two papers:

1) Y. Hong, R. Kwitt, N. Singh, B. Davis, N. Vasconcelos, M. Niethammer. Geodesic Regression on the Grassmannian. ECCV 2014. 

2) Y. Hong, N. Singh, R. Kwitt, M. Niethammer. Time-Warped Geodesic Regression. MICCAI 2014.

For information on how to use this library, see the [wiki](https://bitbucket.org/yi_hong/ggr/wiki/Home).